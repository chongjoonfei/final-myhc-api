<?php
    include_once '../config/db.php';
    include_once '../objects/v1/booking.php';

    // get database connection
    $database = new Database();
    $db = $database->getConnection();

    // prepare object
    $booking = new Booking($db);
    $booking->booking_no = isset($_GET['bNo']) ? $_GET['bNo'] : die();
    $ref_no = isset($_GET['rNo']) ? $_GET['rNo'] : die();
    $result = $booking->readByBookingNoAndRefNo($booking->booking_no.'#'.$ref_no);

    require_once 'receipt.php'; 
    // Include autoloader 
    require_once 'dompdf/autoload.inc.php'; 
    
    // Reference the Dompdf namespace 
    use Dompdf\Dompdf; 
    
    // Instantiate and use the dompdf class 
    $dompdf = new Dompdf();

    //    $file_name = md5(rand()) . '.pdf';
    $html_code = fetch_data($result,"");

    // Load HTML content 
    $dompdf->loadHtml($html_code); 
    
    // (Optional) Setup the paper size and orientation 
    $dompdf->setPaper('A4', 'potrait'); 
    
    // Render the HTML as PDF 
    $dompdf->render(); 

    ob_end_clean();
    
    // Output the generated PDF (1 = download and 0 = preview) 
    $dompdf->stream("codexworld", array("Attachment" => 0));
  
 
 
?>