<?php

include_once 'person.php';

class BookingPatient{
  
    // database connection and table name
    private $conn;
    private $table_name = "booking_patient";

    // object properties
	public $booking_id;
    public $ic_no;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

	// used when filling up the update record form
	function readOne(){

		$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_id = :booking_id
					and ic_no = :ic_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_id", $this->booking_id);
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->booking_id = $row['booking_id'];
		$this->ic_no = $row['ic_no']; 

	}

 	// used when read record by Booking Id
	 function readByBookingId($booking_id){

		$person = new Person($this->conn);
		$this->booking_id=htmlspecialchars(strip_tags($booking_id));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_id = :booking_id
				 ";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_id", $this->booking_id);
		
		// execute query
		$stmt->execute();
		
	
		$arr=array();
		$record_item = null;

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"booking_id" => $booking_id,
				"ic_no" => $ic_no,
				"person" =>  $person->readByIcNo($ic_no)
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					booking_id=:booking_id, 
					ic_no=:ic_no ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
 		
		// bind values
		$stmt->bindParam(":booking_id", $this->booking_id);
		$stmt->bindParam(":ic_no", $this->ic_no); 
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
 

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE booking_id = ? and ic_no = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));

		// bind id of record to delete
		$stmt->bindParam(1, $this->booking_id);
		$stmt->bindParam(2, $this->ic_no);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}
	

}

?>