<?php

class DataSharing{
  
    // database connection and table name
    private $conn;
    private $table_name = "data_sharing";

    // object properties
    public $ic_no;
	public $bmz_updates;
	public $partner_updates;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

   

	// used when filling up the update record form
	function readOne(){

		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ic_no = :ic_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ic_no", $this->ic_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->ic_no = $row['ic_no'];
		$this->bmz_updates = $row['bmz_updates'];
		$this->partner_updates = $row['partner_updates'];
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				ic_no=:ic_no,
				bmz_updates=:bmz_updates,  partner_updates=:partner_updates";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		$this->bmz_updates=htmlspecialchars(strip_tags($this->bmz_updates));
		$this->partner_updates=htmlspecialchars(strip_tags($this->partner_updates));
		
		// bind values
		$stmt->bindParam(":ic_no", $this->ic_no);
		$stmt->bindParam(":bmz_updates", $this->bmz_updates);
		$stmt->bindParam(":partner_updates", $this->partner_updates);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						bmz_updates=:bmz_updates,  partner_updates=:partner_updates
					WHERE
						ic_no=:ic_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
			$this->bmz_updates=htmlspecialchars(strip_tags($this->bmz_updates));
			$this->partner_updates=htmlspecialchars(strip_tags($this->partner_updates));
			
			// bind values
			$stmt->bindParam(":ic_no", $this->ic_no);
			$stmt->bindParam(":bmz_updates", $this->bmz_updates);
			$stmt->bindParam(":partner_updates", $this->partner_updates);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE ic_no = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));

		// bind id of record to delete
		$stmt->bindParam(1, $this->ic_no);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

 
}

?>