<?php
include_once 'constant.php';

class City{
  
    // database connection and table name
    private $conn;
    private $table_name = "postcode";

    // object properties
    
	public $postcode;
	public $area_name;
	public $post_office;
	public $intRecordId;
	public $state_code;
	
	
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				
				ORDER BY
                area_name 
                ";
					
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

    // Read By postcode
    function readByPostcode($postcode)
    {
        $this->postcode=htmlspecialchars(strip_tags($postcode));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					postcode = :postcode
				";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":postcode", $postcode);
		
		// execute query
		$stmt->execute();

		 //return $stmt;
		

		 // get retrieved row
		//$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		// $this->postcode = $row['postcode'];
		// $this->area_name = $row['area_name'];
		// $this->post_office = $row['post_office'];
		// $this->state_code = $row['state_code'];


		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$postcode=array(
				"postcode" => $postcode,
				 "area_name" => $row['area_name'],
				 "post_office" => $row['post_office'],
				 "state_code" => $row['state_code'],
			
			);
			array_push($arr, $postcode);
		}
	  
		return $arr;






		
    }

	
	
}

?>