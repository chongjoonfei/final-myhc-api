<?php

include_once 'booking-product.php';
include_once 'booking-patient.php';
include_once 'person.php';
include_once 'payment.php';
include_once 'screening-center.php';
include_once 'constant.php';
include_once 'screening-center.php';
include_once 'user-account.php';



date_default_timezone_set('Asia/Kuala_Lumpur');

class Booking{
  
    // database connection and table name
    private $conn;
    private $table_name = "booking";

    // object properties
	public $booking_id;
	public $booking_no;
    public $reg_no;
	public $ic_no;
	public $screening_location;
	public $screening_center_id;
	public $screening_date;
	public $screening_time;
	public $date_modified;
	public $status;
	public $remark;

	// public $person;
	// public $products;
	// public $patients;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					booking_no";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_no = :booking_no
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_no", $this->booking_no);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->booking_id = $row['booking_id'];
		$this->booking_no = $row['booking_no'];
		$this->reg_no = $row['reg_no'];
		$this->ic_no = $row['ic_no'];
		$this->screening_location = $row['screening_location'];
		$this->screening_center_id = $row['screening_center_id'];
		$this->screening_date = $row['screening_date'];
		$this->screening_time = $row['screening_time'];
		$this->date_modified = $row['date_modified'];
		$this->status = $row['status'];
		$this->remark = $row['remark'];

	}

 	// used when read record by reg No
	 function readByRegNo($reg_no){

		$bookingProduct = new BookingProduct($this->conn);
		$bookingPatient = new BookingPatient($this->conn);

		$this->reg_no=htmlspecialchars(strip_tags($reg_no));
		
		// query to read records
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					reg_no = :reg_no
				 ";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":reg_no", $this->reg_no);
		
		// execute query
		$stmt->execute();
		
	
		$arr=array();
	
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"reg_no" => $reg_no,
				"booking_id" => $booking_id,
				"booking_no" => $booking_no,
				"ic_no" => $ic_no,
				"screening_location" => $screening_location,
				"screening_center_id" => $screening_center_id,
				"screening_date" => $screening_date,
				"screening_time" => $screening_time,
				"date_modified" => $date_modified,
				"status" => $status,
				"remark" => $remark,
				"products" => $bookingProduct->readByBookingId($booking_id),
				"patients" => $bookingPatient->readByBookingId($booking_id)
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;

	}

	// used when read record by reg No and keyword
	function readByStatusAndRegNo($reg_no,$keyword,$key){

		$bookingProduct = new BookingProduct($this->conn);
		$bookingPatient = new BookingPatient($this->conn);
		$screeningCenter = new ScreeningCenter($this->conn);
		$payment = new Payment($this->conn);

		$this->reg_no=htmlspecialchars(strip_tags($reg_no));
		$keyword = strtoupper($keyword);
		
		// query to read records
		if ($key=='date'){
			$query = "SELECT
				*
			FROM
				booking
			WHERE
				reg_no = ?
				and (status = 'COMPLETED' or status like '%CANCEL%')
				and screening_date like ?
			order by screening_date desc
			";
		}else if ($key=='location'){
			$query = "SELECT * 
				FROM
					booking
				LEFT JOIN screening_center
					ON (sc_id = booking.screening_center_id)
							WHERE
					reg_no = ?
					and (status = 'COMPLETED' or status like '%CANCEL%') 
					and upper(sc_name) like ?";
		}

	
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$keyword=htmlspecialchars(strip_tags($keyword));
		$keyword = "%{$keyword}%";
		
		// bind
		$stmt->bindParam(1, $this->reg_no);
		$stmt->bindParam(2, $keyword);
 		
		// execute query
		$stmt->execute();
		
	
		$arr=array();
	
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"reg_no" => $reg_no,
				"booking_id" => $booking_id,
				"booking_no" => $booking_no,
				"ic_no" => $ic_no,
				"screening_location" => $screening_location,
				"screening_center_id" => $screening_center_id,
				"screening_center" => $screeningCenter->readById($screening_center_id),
				"screening_date" => $screening_date,
				"screening_time" => $screening_time,
				"date_modified" => $date_modified,
				"status" => $status,
				"remark" => $remark,
				"products" => $bookingProduct->readByBookingId($booking_id),
				"patients" => $bookingPatient->readByBookingId($booking_id),
				"payments" => $payment->readByBookingNo($booking_no)
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}

	function readByBookingNo(){

		$bookingProduct = new BookingProduct($this->conn);
		$bookingPatient = new BookingPatient($this->conn);
		$person = new Person($this->conn);
		$payment = new Payment($this->conn);
		$screeningCenter = new ScreeningCenter($this->conn);
		$userAccount = new UserAccount($this->conn);

		$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
		
		// query to read records
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_no = :booking_no
				 ";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_no", $this->booking_no);
		
		// execute query
		$stmt->execute();
		
		$record_item=null;
	
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$record_item=array(
			"reg_no" => $row['reg_no'],
			"booking_id" => $row['booking_id'],
			"booking_no" => $row['booking_no'],
			"ic_no" => $row['ic_no'],
			"person" => $person->readByIcNo($row['ic_no']),
			"userAccount" => $userAccount->readByIcNo($row['ic_no']),
			"screening_location" => $row['screening_location'],
			"screening_center_id" => $row['screening_center_id'],
			"screening_center" => $screeningCenter->readById($row['screening_center_id']),
			"screening_date" => $row['screening_date'],
			"screening_time" => $row['screening_time'],
			"date_modified" => $row['date_modified'],
			"status" => $row['status'],
			"remark" => $row['remark'],
			"products" => $bookingProduct->readByBookingId($row['booking_id']),
			"patients" => $bookingPatient->readByBookingId($row['booking_id']),
			"payments" => $payment->readByBookingNo($row['booking_no'])
		);
	  
		return $record_item;

	}

	function readByBookingNoAndRefNo($ref_no){

		$bookingProduct = new BookingProduct($this->conn);
		$bookingPatient = new BookingPatient($this->conn);
		$person = new Person($this->conn);
		$payment = new Payment($this->conn);
		$screeningCenter = new ScreeningCenter($this->conn);

		$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
	 
		
		// query to read records
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_no = :booking_no
				 ";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_no", $this->booking_no);
		
		// execute query
		$stmt->execute();
		
		$record_item=null;
	
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$record_item=array(
			"reg_no" => $row['reg_no'],
			"booking_id" => $row['booking_id'],
			"booking_no" => $row['booking_no'],
			"ic_no" => $row['ic_no'],
			"person" => $person->readByIcNo($row['ic_no']),
			"screening_location" => $row['screening_location'],
			"screening_center_id" => $row['screening_center_id'],
			"screening_center" => $screeningCenter->readById($row['screening_center_id']),
			"screening_date" => $row['screening_date'],
			"screening_time" => $row['screening_time'],
			"date_modified" => $row['date_modified'],
			"status" => $row['status'],
			"remark" => $row['remark'],
			"products" => $bookingProduct->readByRefNo($ref_no),
			"patients" => $bookingPatient->readByBookingId($row['booking_id']),
			"payments" => $payment->readByRefNo($ref_no)
		);
	  
		return $record_item;

	}

	function readByRefNo($ref_no){

		$arr_ref = explode("#", $ref_no);
		$booking_no = $arr_ref[0];

		$bookingProduct = new BookingProduct($this->conn);
		$bookingPatient = new BookingPatient($this->conn);
		$person = new Person($this->conn);
		$payment = new Payment($this->conn);
		$screeningCenter = new ScreeningCenter($this->conn);

		$this->booking_no=htmlspecialchars(strip_tags($booking_no));
		
		// query to read records
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_no = :booking_no
				order by screening_date desc	
				 ";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_no", $this->booking_no);
		
		// execute query
		$stmt->execute();
		
		$record_item=null;
	
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$record_item=array(
			"reg_no" => $row['reg_no'],
			"booking_id" => $row['booking_id'],
			"booking_no" => $row['booking_no'],
			"ic_no" => $row['ic_no'],
			"person" => $person->readByIcNo($row['ic_no']),
			"screening_location" => $row['screening_location'],
			"screening_center_id" => $row['screening_center_id'],
			"screening_center" => $screeningCenter->readById($row['screening_center_id']),
			"screening_date" => $row['screening_date'],
			"screening_time" => $row['screening_time'],
			"date_modified" => $row['date_modified'],
			"status" => $row['status'],
			"remark" => $row['remark'],
			"products" => $bookingProduct->readByRefNo($ref_no),
			"patients" => $bookingPatient->readByBookingId($row['booking_id']),
			"payments" => $payment->readByBookingNo($row['booking_no'])
		);
	  
		return $record_item;

	}

	function readByBookingNoForTestForm(){

		$bookingProduct = new BookingProduct($this->conn);
		$bookingPatient = new BookingPatient($this->conn);
		$person = new Person($this->conn);
		$screeningCenter = new ScreeningCenter($this->conn);
		$userAccount = new UserAccount($this->conn);

		$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
		
		// query to read records
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_no = :booking_no
				 ";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_no", $this->booking_no);
		
		// execute query
		$stmt->execute();
		
		$record_item=null;
	
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$record_item=array(
			"reg_no" => $row['reg_no'],
			"booking_id" => $row['booking_id'],
			"booking_no" => $row['booking_no'],
			"ic_no" => $row['ic_no'],
			"person" => $person->readByIcNo($row['ic_no']),
			"userAccount" => $userAccount->readByIcNo($row['ic_no']),
			"screening_location" => $row['screening_location'],
			"screening_center_id" => $row['screening_center_id'],
			"screening_center" => $screeningCenter->readById($row['screening_center_id']),
			"screening_date" => $row['screening_date'],
			"screening_time" => $row['screening_time'],
			"date_modified" => $row['date_modified'],
			"status" => $row['status'],
			"remark" => $row['remark'],
			"products" => $bookingProduct->readByBookingIdForTestForm($row['booking_id']),
			"patients" => $bookingPatient->readByBookingId($row['booking_id'])
		 
		);
	  
		return $record_item;

	}

 
	// used when read record by upcoming booked date
	function readUpcomingByRegNo($reg_no){

		$bookingProduct = new BookingProduct($this->conn);
		$bookingPatient = new BookingPatient($this->conn);
		$payment = new Payment($this->conn);

		$this->reg_no=htmlspecialchars(strip_tags($reg_no));
		
		// query to read records
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					reg_no = :reg_no
					and status = 'PENDING'
					and booking_no in (select booking_no from payment) 
					
				";
					// and booking_no in (select booking_no from payment)
					//and DATE_FORMAT(screening_date, '%Y-%m-%d')<= DATE_FORMAT(CURRENT_DATE, '%Y-%m-%d')

					// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":reg_no", $this->reg_no);
		
		// execute query
		$stmt->execute();
		
	
		$arr=array();
	
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"reg_no" => $reg_no,
				"booking_id" => $booking_id,
				"booking_no" => $booking_no,
				"ic_no" => $ic_no,
				"screening_location" => $screening_location,
				"screening_center_id" => $screening_center_id,
				"screening_date" => $screening_date,
				"screening_time" => $screening_time,
				"date_modified" => $date_modified,
				"status" => $status,
				"remark" => $remark,
				"products" => $bookingProduct->readByBookingId($booking_id),
				"patients" => $bookingPatient->readByBookingId($booking_id),
				"payments" => $payment->readByBookingNo($booking_no)
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}


	// used when read record by screening date range and timeslot
	function readCountByScreeningDateRange($centerId,$startDate, $endDate){

		$startDate=htmlspecialchars(strip_tags($startDate));
		$endDate=htmlspecialchars(strip_tags($endDate));
		$this->screening_center_id=htmlspecialchars(strip_tags($centerId));
		
		// query to read records
		$query = "SELECT
					screening_center_id, screening_date, screening_time, count(screening_time) as total_count
				FROM
					" . $this->table_name . "  
				WHERE
					screening_center_id = :screening_center_id
					and screening_date between :startDate and :endDate
					and status='PENDING'
				group by screening_center_id, screening_date, screening_time";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":startDate", $startDate);
		$stmt->bindParam(":endDate", $endDate);
		$stmt->bindParam(":screening_center_id", $this->screening_center_id);
		
		// execute query
		$stmt->execute();
		
	
		$arr=array();
	
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"screening_center_id" => $screening_center_id,
				"screening_date" => $screening_date,
				"screening_time" => $screening_time,
				"total_count" => $total_count 
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}

	// used when read record by screening date and timeslot
	function readCountTimeslotByScreeningDate($centerId,$sDate){

		$sDate=htmlspecialchars(strip_tags($sDate));
		$this->screening_center_id=htmlspecialchars(strip_tags($centerId));
		
		// query to read records
		$query = "SELECT
					screening_center_id, screening_date, screening_time, count(screening_time) as total_count
				FROM
					" . $this->table_name . "  
				WHERE
					screening_center_id = :screening_center_id
					and screening_date = :sDate  
					and  status='PENDING'
				group by screening_center_id, screening_date, screening_time
					";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":sDate", $sDate);
		$stmt->bindParam(":screening_center_id", $this->screening_center_id);
		
		// execute query
		$stmt->execute();
		
	
		$arr=array();
	
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"screening_center_id" => $screening_center_id,
				"screening_date" => $screening_date,
				"screening_time" => $screening_time,
				"total_count" => $total_count 
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}	


	// used when read record by screening date and timeslot
	function readByScreeningDateAndTimeslot($centerId,$sDate, $timeslot){

		$this->screening_date=htmlspecialchars(strip_tags($sDate));
		$this->screening_time=htmlspecialchars(strip_tags($timeslot));
		$this->screening_center_id=htmlspecialchars(strip_tags($centerId));
		
		// query to read records
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					screening_center_id = :screening_center_id
					and screening_date = :screening_date
					and screening_time = :screening_time 
					and  status='PENDING'
				";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":screening_date", $this->screening_date);
		$stmt->bindParam(":screening_time", $this->screening_time);
		$stmt->bindParam(":screening_center_id", $this->screening_center_id);
		
		// execute query
		$stmt->execute();
		
	
		$arr=array();
	
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"reg_no" => $reg_no,
				"booking_id" => $booking_id,
				"booking_no" => $booking_no,
				"ic_no" => $ic_no,
				"screening_location" => $screening_location,
				"screening_center_id" => $screening_center_id,
				"screening_date" => $screening_date,
				"screening_time" => $screening_time,
				"date_modified" => $date_modified,
				"status" => $status
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					reg_no=:reg_no,  booking_no=:booking_no, booking_id=:booking_id, 
					ic_no=:ic_no, screening_location=:screening_location,
					screening_center_id=:screening_center_id, screening_date=:screening_date,
					screening_time=:screening_time,date_modified=:date_modified, status=:status ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
		$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
		$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
		$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
		$this->screening_location=htmlspecialchars(strip_tags($this->screening_location));
		$this->screening_center_id=htmlspecialchars(strip_tags($this->screening_center_id));
		$this->screening_date=htmlspecialchars(strip_tags($this->screening_date));
		$this->screening_time=htmlspecialchars(strip_tags($this->screening_time));
		$this->date_modified=htmlspecialchars(strip_tags($this->date_modified));
		$this->status ="PENDING";
		
		// bind values
		$stmt->bindParam(":reg_no", $this->reg_no);
		$stmt->bindParam(":booking_id", $this->booking_id);
		$stmt->bindParam(":booking_no", $this->booking_no);
		$stmt->bindParam(":ic_no", $this->ic_no);
		$stmt->bindParam(":screening_location", $this->screening_location);
		$stmt->bindParam(":screening_center_id", $this->screening_center_id);
		$stmt->bindParam(":screening_date", $this->screening_date);
		$stmt->bindParam(":screening_time", $this->screening_time);
		$stmt->bindParam(":date_modified", $this->date_modified);
		$stmt->bindParam(":status", $this->status);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
		date_default_timezone_set('Asia/Kuala_Lumpur');
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						reg_no=:reg_no, booking_id=:booking_id,
						booking_no=:booking_no, ic_no=:ic_no,
						screening_location=:screening_location, 
						screening_center_id=:screening_center_id, 
						screening_date=:screening_date,	screening_time=:screening_time, 
						date_modified=:date_modified , status=:status, remark=:remark
					WHERE
						booking_no=:booking_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->reg_no=htmlspecialchars(strip_tags($this->reg_no));
			$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
			$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));
			$this->ic_no=htmlspecialchars(strip_tags($this->ic_no));
			$this->screening_location=htmlspecialchars(strip_tags($this->screening_location));
			$this->screening_center_id=htmlspecialchars(strip_tags($this->screening_center_id));
			$this->screening_date=htmlspecialchars(strip_tags($this->screening_date));
			$this->screening_time=htmlspecialchars(strip_tags($this->screening_time));

		$this->date_modified=htmlspecialchars(strip_tags(date("Y-m-d h:i:s")/*gmdate('Y-m-d H:i:s')*/));
			$this->status=htmlspecialchars(strip_tags($this->status));
			$this->remark=htmlspecialchars(strip_tags($this->remark));
			
			// bind values
			$stmt->bindParam(":reg_no", $this->reg_no);
			$stmt->bindParam(":booking_id", $this->booking_id);
			$stmt->bindParam(":booking_no", $this->booking_no);
			$stmt->bindParam(":ic_no", $this->ic_no);
			$stmt->bindParam(":screening_location", $this->screening_location);
			$stmt->bindParam(":screening_center_id", $this->screening_center_id);
			$stmt->bindParam(":screening_date", $this->screening_date);
			$stmt->bindParam(":screening_time", $this->screening_time);
			$stmt->bindParam(":date_modified", $this->date_modified);
			$stmt->bindParam(":status", $this->status);
			$stmt->bindParam(":remark", $this->remark);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE booking_id = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->booking_id=htmlspecialchars(strip_tags($this->booking_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->booking_id);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}
	

	// search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_id LIKE ? OR ic_no LIKE ?   OR reg_no LIKE ?  
				ORDER BY
					booking_id";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
		$stmt->bindParam(3, $keywords);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

		// search records by status
	function searchByStatus($keyword,$status){
	
		$arr_status = explode(",", $status);

		if (sizeof($arr_status) > 1) 
			$str_status = "";
		else
			$str_status = " status = '". $status . "' ";

		foreach ($arr_status as $element) {
			if ($str_status=="")
				$str_status = " status ='". $element . "' ";
			else
				$str_status .= " or  status ='". $element . "' ";
		}

		if ($str_status!="") $str_status = " and (".$str_status.") ";


		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					(booking_no LIKE ? OR ic_no LIKE ?   OR reg_no LIKE ?)
					 ". $str_status ."
				order by screening_date desc";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keyword=htmlspecialchars(strip_tags($keyword));
		$keyword = "%{$keyword}%";

		// $status=htmlspecialchars(strip_tags($status));
		// $status = "%{$status}%";
		
		// bind
		$stmt->bindParam(1, $keyword);
		$stmt->bindParam(2, $keyword);
		$stmt->bindParam(3, $keyword);
		// $stmt->bindParam(4, $status);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}


	function getNextId(){
		$query = "SELECT AUTO_INCREMENT
		FROM information_schema.TABLES
		WHERE TABLE_SCHEMA = '".CONST_DB_NAME."'
		AND TABLE_NAME = '". $this->table_name ."'" ;

		// prepare query
		$stmt = $this->conn->prepare($query);

		if ($stmt->execute()){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			return $row['AUTO_INCREMENT'];
		}else{
			return -1;
		}	
		
	}


	function sendEmail(){

	}
}

?>