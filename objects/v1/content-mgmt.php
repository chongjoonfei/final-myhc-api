<?php

class ContentMgmt{
  
    // database connection and table name
    private $conn;
    private $table_name = "content_mgmt";

    // object properties
    
	public $id;
	public $name;
	public $content;
	public $date_updated;

  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					name";
					
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}

	// create object
	// got auto increment number
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				 id=:id,
				 name=:name, 
				 content=:content, 
				 date_updated=:date_updated				
				 ";
					

		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->id=htmlspecialchars(strip_tags($this->id));
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->content=htmlspecialchars(strip_tags($this->content));
		$this->date_updated=htmlspecialchars(strip_tags($this->date_updated));
		
		
		// bind values
		$stmt->bindParam(":id", $this->id);
		$stmt->bindParam(":name", $this->name);
		$stmt->bindParam(":content", $this->content);
		$stmt->bindParam(":date_updated", $this->date_updated);
	
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
		
	// used when filling up the update record form
	function readOne(){
		
		$this->name=htmlspecialchars(strip_tags($this->name));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					name = :name
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated 
		$stmt->bindParam(":name", $this->name);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->id = $row['id'];
		$this->name = $row['name']; 
		$this->content= $row['content'];
		$this->date_updated= $row['date_updated'];	
		
	}
	

	// update the record except primary key is auto increment no:id
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
					
					content=:content, 
					date_updated=:date_updated
				   
					WHERE
						name = :name";
						
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
				
			$this->name=htmlspecialchars(strip_tags($this->name));
			$this->content=htmlspecialchars(strip_tags($this->content));
			$this->date_updated=htmlspecialchars(strip_tags($this->date_updated));
			
			

			
			// bind values

			$stmt->bindParam(":name", $this->name);
			$stmt->bindParam(":content", $this->content);
			$stmt->bindParam(":date_updated", $this->date_updated);
			
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	// delete the record based on the primary key, add PK if more than 1
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE name = ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->name=htmlspecialchars(strip_tags($this->name));
		
		// bind id of record to delete
		$stmt->bindParam(1, $this->name);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					name LIKE ? OR content LIKE ?  
				ORDER BY
					name";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

	
}

?>