<?php


class PatientType{
  
    // database connection and table name
    private $conn;
    private $table_name = "patient_type";

    // object properties
    public $patient_type_code;
	public $name;
	public $min_age;
	public $max_age;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
				patient_type_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}
 
	
}

?>