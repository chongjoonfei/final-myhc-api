<?php
include_once 'ipay88-payment-method.php';

class Payment{
  
    // database connection and table name
    private $conn;
    private $table_name = "payment";

    // object properties
	public $trans_no;
    public $payment_id;
	public $payment_date;
	public $ref_no;
	public $booking_no;
	public $amount;
	public $status;
	public $remark;
 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					payment_id";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->code=htmlspecialchars(strip_tags($this->code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					payment_id = :payment_id
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":payment_id", $this->payment_id);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// set values to object properties
		$this->trans_no = $row['trans_no'];
		$this->payment_id = $row['payment_id'];
		$this->payment_date = $row['payment_date'];
		$this->booking_no = $row['booking_no'];
		$this->ref_no = $row['ref_no'];
		$this->amount = $row['amount'];
 		$this->status = $row['status'];
		$this->remark = $row['remark'];
 
	}

	// used when read record by Booking No
	function readByBookingNo($booking_no){

		$payment_method = new iPay88PaymentMethod($this->conn);

		$this->booking_no=htmlspecialchars(strip_tags($booking_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					booking_no = :booking_no
					";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":booking_no", $this->booking_no);
		
		// execute query
		$stmt->execute();
		
	
		$record_item = null;
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"trans_no" => $trans_no,
				"payment_id" => $payment_id,
				"ipay88" => $payment_method->readByPaymentId($payment_id),
				"payment_date" => $payment_date,
				"booking_no" => $booking_no,
				"ref_no" => $ref_no,
				"amount" => $amount,
				"status" => $status,
				"remark" => $remark 
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}

 

	// used when read record by Booking No
	function readByRefNo($ref_no){

		$payment_method = new iPay88PaymentMethod($this->conn);

		$this->ref_no=htmlspecialchars(strip_tags($ref_no));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ref_no = :ref_no
					";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ref_no", $this->ref_no);
		
		// execute query
		$stmt->execute();
		
	
		$record_item = null;
		$arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"trans_no" => $trans_no,
				"payment_id" => $payment_id,
				"ipay88" => $payment_method->readByPaymentId($payment_id),
				"payment_date" => $payment_date,
				"booking_no" => $booking_no,
				"ref_no" => $ref_no,
				"amount" => $amount,
				"status" => $status,
				"remark" => $remark 
			);
			array_push($arr, $record_item);
		}
		
		return $arr;

	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET trans_no = :trans_no, 
				 payment_id = :payment_id,  payment_date = :payment_date, booking_no = :booking_no,
				 ref_no = :ref_no, amount = :amount,  status = :status,
				 remark = :remark ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->trans_no=htmlspecialchars(strip_tags($this->trans_no));
		$this->payment_id=htmlspecialchars(strip_tags($this->payment_id));
		$this->payment_date=htmlspecialchars(strip_tags($this->payment_date));
		$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
		$this->ref_no=htmlspecialchars(strip_tags($this->ref_no));
		$this->amount=htmlspecialchars(strip_tags($this->amount));
 		$this->status=htmlspecialchars(strip_tags($this->status));
		$this->remark=htmlspecialchars(strip_tags($this->remark));
 
		
		// bind values
		$stmt->bindParam(":trans_no", $this->trans_no);
		$stmt->bindParam(":payment_id", $this->payment_id);
		$stmt->bindParam(":payment_date", $this->payment_date);
		$stmt->bindParam(":booking_no", $this->booking_no);
		$stmt->bindParam(":ref_no", $this->ref_no);
		$stmt->bindParam(":amount", $this->amount);
 		$stmt->bindParam(":status", $this->status);
		$stmt->bindParam(":remark", $this->remark);
 
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET payment_id = :payment_id
						payment_date = :payment_date, booking_no = :booking_no,
						ref_no = :ref_no, amount = :amount,  status = :status,
						remark = :remark 
					WHERE
						trans_no = :trans_no";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->trans_no=htmlspecialchars(strip_tags($this->trans_no));
			$this->payment_id=htmlspecialchars(strip_tags($this->payment_id));
			$this->payment_date=htmlspecialchars(strip_tags($this->payment_date));
			$this->booking_no=htmlspecialchars(strip_tags($this->booking_no));
			$this->ref_no=htmlspecialchars(strip_tags($this->ref_no));
			$this->amount=htmlspecialchars(strip_tags($this->amount));
			$this->status=htmlspecialchars(strip_tags($this->status));
			$this->remark=htmlspecialchars(strip_tags($this->remark));
			
			// bind values
			$stmt->bindParam(":trans_no", $this->trans_no);
			$stmt->bindParam(":payment_id", $this->payment_id);
			$stmt->bindParam(":payment_date", $this->payment_date);
			$stmt->bindParam(":booking_no", $this->booking_no);
			$stmt->bindParam(":ref_no", $this->ref_no);
			$stmt->bindParam(":amount", $this->amount);
			$stmt->bindParam(":status", $this->status);
			$stmt->bindParam(":remark", $this->remark);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE payment_id = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->payment_id=htmlspecialchars(strip_tags($this->payment_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->payment_id);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	// search records
	function search($keywords){
	
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					payment_desc LIKE ? OR ref_no LIKE ?  
				ORDER BY
					trans_no";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}
	
}

?>