<?php


class EmailMgmt{
  
    // database connection and table name
    private $conn;
    private $table_name = "email_mgmt";

    // object properties
    public $code;
	public $title;
	public $content;
	public $sender;

	//for sending messages properties
	public $paramValues;
	public $receivers;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->code=htmlspecialchars(strip_tags($this->code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					code = :code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":code", $this->code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->code = $row['code'];
		$this->title = $row['title'];
		$this->content = $row['content'];
		$this->sender = $row['sender'];
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				code=:code,  title=:title, content=:content";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));
		$this->title=htmlspecialchars(strip_tags($this->title));
		$this->content=htmlspecialchars(strip_tags($this->content));
		
		// bind values
		$stmt->bindParam(":code", $this->code);
		$stmt->bindParam(":title", $this->title);
		$stmt->bindParam(":content", $this->content);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						title = :title,
						content = :content,
						sender = :sender
					WHERE
						code = :code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->code=htmlspecialchars(strip_tags($this->code));
			$this->title=htmlspecialchars(strip_tags($this->title));
			$this->content=htmlspecialchars(strip_tags($this->content));
			$this->sender=htmlspecialchars(strip_tags($this->sender));
			
			// bind values
			$stmt->bindParam(":code", $this->code);
			$stmt->bindParam(":title", $this->title);
			$stmt->bindParam(":content", $this->content);
			$stmt->bindParam(":sender", $this->sender);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE code = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->code=htmlspecialchars(strip_tags($this->code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	function sendMail(){
		$content = $this->content;

		foreach ($this->paramValues as $paramValue) {
			$content = str_replace($paramValue['param'],$paramValue['value'],$content);
		}

		if (mail($this->receivers,$this->title,$content,"From: ". $this->sender)){
			return true;
		}else{
			return false;
		}
	}

	function readMailContent(){
		$content = $this->content;

		foreach ($this->paramValues as $paramValue) {
			$content = str_replace($paramValue['param'],$paramValue['value'],$content);
		}

		 return $content;
	}
	
}

?>