<?php
mysqli_report(MYSQLI_REPORT_ALL);
error_reporting(E_ALL);

include_once 'person.php';
include_once 'constant.php';
 

class MessageBox{

    //database connection and table-name
    private $conn;
    private $table_name = "message_box";

    //object properties
    public $message_id; //PK
    public $sender;
    public $sender_name;
    public $receiver;
    public $receiver_name;
    public $subject;
    public $content;
    public $headers;
    public $date_sent;
    public $message_type_code; //FK :  SUPPORT, ENQUIRY, INBOX
    public $status; 
    public $attachment;
    public $message_root_id;
    public $latest;

    //for sending messages properties
	public $paramValues;
	public $receivers;

    //constructor with $db
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records for receiver
    function readByReceiver($username){
        // select all query
		$query = "SELECT
                    *
                FROM
                message_box  
                where receiver =:username  
                and message_root_id = 0  
                ORDER BY
                date_sent desc";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":username", $username);

        // execute query
        $stmt->execute();

        $arr=array();

        $person = new Person($this->conn);

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
			$record_item=array(
				"message_id" => $message_id,
				"sender" => $sender,
                "sender_name" => $sender_name,
				"receiver" => $receiver,
                "receiver_name" => $receiver_name,
				"subject" => $subject,
				"content" => $content,  
				"headers" => $headers,
				"date_sent" => $date_sent,
				"message_type_code" => $message_type_code,
                "status" => $status,
                "attachment" => $attachment,
                "message_root_id" => $message_root_id
                // "submessages" => $this->readByMessageRootId($message_id)
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;

    }

       // read all records for receiver
       function readAllMessages($username){
        // select all query
		$query = "SELECT
                    *
                FROM
                message_box  
                where (receiver =:username or  sender =:username)
                and latest = 1
                ORDER BY
                date_sent desc";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":username", $username);

        // execute query
        $stmt->execute();

        $arr=array();

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row);
            $id = $message_root_id;
            if ($message_root_id==0) $id = $message_id;  
			$record_item=array(
				"message_id" => $message_id,
				"sender" => $sender,
                "sender_name" => $sender_name,
				"receiver" => $receiver,
                "receiver_name" => $receiver_name,
				"subject" => $subject,
				"content" => $content,  
				"headers" => $headers,
				"date_sent" => $date_sent,
				"message_type_code" => $message_type_code,
                "status" => $status,
                "attachment" => $attachment,
                "message_root_id" => $message_root_id,
                "history" => $this->readByMessageHistory($id)
			);
			array_push($arr, $record_item);
		}
	  
		return $arr;

    }


    // read all records for submessages
    function readByMessageHistory($message_id){
        // select all query
        $query = "SELECT
                    *
                FROM
                    message_box 
                where (message_root_id = ? or message_id = ?)   
                ORDER BY
                date_sent asc";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(1, $message_id);
        $stmt->bindParam(2, $message_id);

        // execute query
        $stmt->execute();
        $arr=array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $record_item=array(
                "message_id" => $message_id,
                "sender" => $sender,
                "sender_name" => $sender_name,
                "receiver" => $receiver,
                "receiver_name" => $receiver_name,
                "subject" => $subject,
                "content" => $content,  
                "headers" => $headers,
                "date_sent" => $date_sent,
                "message_type_code" => $message_type_code,
                "status" => $status,
                "attachment" => $attachment,
                "message_root_id" => $message_root_id
            );
            array_push($arr, $record_item);
        }
        
        return $arr;
    }

    // read all records for submessages
    function readByMessageRootId($message_root_id){
        // select all query
        $query = "SELECT
                    *
                FROM
                    message_box 
                where message_root_id = :message_root_id    
                ORDER BY
                date_sent desc";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":message_root_id", $message_root_id);

        // execute query
        $stmt->execute();
        $arr=array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $record_item=array(
                "message_id" => $message_id,
                "sender" => $sender,
                "sender_name" => $sender_name,
                "receiver" => $receiver,
                "receiver_name" => $receiver_name,
                "subject" => $subject,
                "content" => $content,  
                "headers" => $headers,
                "date_sent" => $date_sent,
                "message_type_code" => $message_type_code,
                "status" => $status,
                "attachment" => $attachment,
                "message_root_id" => $message_root_id
            );
            array_push($arr, $record_item);
        }
        
        return $arr;
    }

    // read all records for message-type
    function readByMessageType($message_type_code){
        // select all query
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                where
                    message_type_code = :message_type_code
                    and message_root_id = 0
                ORDER BY
                    date_sent desc";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        $arr=array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $record_item=array(
                "message_id" => $message_id,
                "sender" => $sender,
                "sender_name" => $sender_name,
                "receiver" => $receiver,
                "receiver_name" => $receiver_name,
                "subject" => $subject,
                "content" => $content,  
                "headers" => $headers,
                "date_sent" => $date_sent,
                "message_type_code" => $message_type_code,
                "status" => $status,
                "attachment" => $attachment,
                "message_root_id" => $message_root_id,
                "submessages" => $this->readByMessageRootId($message_id)
            );
            array_push($arr, $record_item);
        }
        
        return $arr;
    }
 

    function sendMail(){
        $content = $this->message;

        foreach ($this->paramValues as $paramValue) {
            $content = str_replace($paramValue['param'],$paramValue['value'],$message);
        }

        if (mail($this->receiver,$this->title,$message,"From: ". $this->sender)){
            return true;
        }else{
            return false;
        }
    }

        
    function create(){

        
        if ($this->message_root_id!=0){
            $query = "update message_box set latest=0 where message_id = ? or message_root_id=?";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $this->message_root_id);
            $stmt->bindParam(2, $this->message_root_id);
            $stmt->execute();
        }


        $query = "INSERT INTO 
                    message_box
                    SET 
                        message_id=:message_id,
                        sender=:sender, receiver=:receiver,
                        sender_name=:sender_name, receiver_name=:receiver_name,
                        subject=:subject,
                        content=:content, headers=:headers, date_sent=:date_sent,
                        message_type_code=:message_type_code,  
                        status=:status, attachment = :attachment, message_root_id = :message_root_id, latest=1
                    ";

        //prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->message_id=htmlspecialchars(strip_tags($this->message_id));
        $this->sender=htmlspecialchars(strip_tags($this->sender));
        $this->receiver=htmlspecialchars(strip_tags($this->receiver));
        $this->sender_name=htmlspecialchars(strip_tags($this->sender_name));
        $this->receiver_name=htmlspecialchars(strip_tags($this->receiver_name));
        $this->subject=htmlspecialchars(strip_tags($this->subject));
        $this->content=htmlspecialchars(strip_tags($this->content));
        $this->headers=htmlspecialchars(strip_tags($this->headers));
        $this->date_sent=htmlspecialchars(strip_tags($this->date_sent));
        $this->message_type_code=htmlspecialchars(strip_tags($this->message_type_code));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->attachment=htmlspecialchars(strip_tags($this->attachment));
        $this->message_root_id=htmlspecialchars(strip_tags($this->message_root_id));

        $person = new Person($this->conn);
        $person->readOneByUsername($this->receiver);

        // bind values
        $stmt->bindParam(":message_id", $this->message_id);
        $stmt->bindParam(":sender", $this->sender);
        $stmt->bindParam(":receiver", $this->receiver);
        $stmt->bindParam(":sender_name", $this->sender_name);
        $stmt->bindParam(":receiver_name", $person->name);
        $stmt->bindParam(":subject", $this->subject);
        $stmt->bindParam(":content", $this->content);
        $stmt->bindParam(":headers", $this->headers);	
        $stmt->bindParam(":date_sent", $this->date_sent);
        $stmt->bindParam(":message_type_code", $this->message_type_code);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":attachment", $this->attachment);		
        $stmt->bindParam(":message_root_id", $this->message_root_id);		
     
		
		// execute query
		if($stmt->execute()){

			return true;
		}else{
			return false;
		}
    }

    function getNextId(){
		$query = "SELECT AUTO_INCREMENT
		FROM information_schema.TABLES
		WHERE TABLE_SCHEMA = '".CONST_DB_NAME."'
		AND TABLE_NAME = '". $this->table_name ."'" ;

		// prepare query
		$stmt = $this->conn->prepare($query);

		if ($stmt->execute()){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			return $row['AUTO_INCREMENT'];
		}else{
			return -1;
		}	
		
	}


    // read all records
    function readAll(){
        // select all query
		$query = "SELECT
                    *
                FROM
                    " . $this->table_name . "  
                where message_root_id =0
                ORDER BY
                    message_id";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }
    
    

    //ada prob
    function readOne() {
        // query to read single record
		$query = "SELECT
					*
				FROM
					message_box 
				WHERE
                    message_id = :message_id 
                    and message_root_id =0
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key, 2 PK
		$stmt->bindParam(":message_id", $this->message_id);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->message_id = $row['message_id'];
		$this->sender = $row['sender']; 
		$this->receiver= $row['receiver'];
        $this->sender = $row['sender_name']; 
		$this->receiver= $row['receiver_name'];
        $this->subject = $row['subject'];
		$this->content = $row['content']; 
		$this->headers= $row['headers'];
        $this->date_sent = $row['date_sent'];
		$this->message_type_code = $row['message_type_code']; 
        $this->status = $row['status']; 
		$this->attachment= $row['attachment'];
        $this->message_root_id= $row['message_root_id'];
    }

    // delete the record based on the primary key, add PK if more than 1
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE message_id = ?  ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->message_id=htmlspecialchars(strip_tags($this->message_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->message_id);

		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}

        // delete the record based on the primary key, add PK if more than 1
	function deleteRootId(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE message_root_id = ?  ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->message_id=htmlspecialchars(strip_tags($this->message_root_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->message_root_id);

		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}

   // search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
                    subject LIKE ? OR message_type_code LIKE ?  
                    and message_root_id is null
				ORDER BY
				    date_sent desc";

		
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}

    function update(){

    }


}
?>