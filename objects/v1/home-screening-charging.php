<?php

class HomeScreeningCharging{
  
    // database connection and table name
    private $conn;
    private $table_name = "ref_home_screening_charging";

    // object properties
    public $id;
	public $code;
	public $description;
	public $price;
	public $quantity;
	public $enabled;

	
	
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					code";
					
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}
	
}

?>