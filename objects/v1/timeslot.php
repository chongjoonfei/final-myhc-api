<?php

class Timeslot{
  
    // database connection and table name
    private $conn;
    private $table_name = "timeslot";

    // object properties
    public $ts_code;
	public $ts_description;
 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					ts_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->code=htmlspecialchars(strip_tags($this->ts_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ts_code = :ts_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":ts_code", $this->ts_code);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->ts_code = $row['ts_code'];
		$this->ts_description = $row['ts_description'];
 
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
					ts_code=:ts_code,  ts_description=:ts_description ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->ts_code=htmlspecialchars(strip_tags($this->ts_code));
		$this->ts_description=htmlspecialchars(strip_tags($this->ts_description));
		
		// bind values
		$stmt->bindParam(":ts_code", $this->ts_code);
		$stmt->bindParam(":ts_description", $this->ts_description); 
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						 ts_description=:ts_description 
					WHERE
						ts_code=:ts_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->ts_code=htmlspecialchars(strip_tags($this->ts_code));
			$this->ts_description=htmlspecialchars(strip_tags($this->ts_description));
 
			
			// bind values
			$stmt->bindParam(":ts_code", $this->ts_code);
			$stmt->bindParam(":ts_description", $this->ts_description);
 
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE ts_code = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->ts_code=htmlspecialchars(strip_tags($this->ts_code));

		// bind id of record to delete
		$stmt->bindParam(1, $this->ts_code);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	// search records
	function search($keywords){
	
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					ts_code LIKE ?  
				ORDER BY
					ts_code";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);

		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}
	
}

?>