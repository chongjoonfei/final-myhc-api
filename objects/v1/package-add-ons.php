<?php

include_once 'add-on.php';

class PackageAddOns{
  
    // database connection and table name
    private $conn;
    private $table_name = "package_add_ons";

    // object properties
    
	public $package_code;
	public $add_on_code;
	public $add_on_name;
	public $test_location_code;
	public $test_location_name;
	public $total_test_conducted;
	public $patient_type_code;



  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					add_on_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}



	function readByPackageCode($package_code){

		$addOn = new AddOn($this->conn);

		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . " 
					
				WHERE
					package_code = :package_code

				ORDER BY
					patient_type_code";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	   
		// bind code of data to be updated
		$stmt->bindParam(":package_code", $package_code);

		// execute query
		$stmt->execute();

		$arr=array();
		//read line by line
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			extract($row); //read every field in the row
			$packageAddOns_item=array(
				"package_code" => $package_code,
				"add_on_code" => $add_on_code,
				"add_on_name" => $add_on_name,
				"test_location_code" => $test_location_code,
				"test_location_name" => $test_location_name,
				"total_test_conducted"  => $total_test_conducted,
				"patient_type_code" => $patient_type_code,
				"add_on" => $addOn->readByAddOnCode($add_on_code)  //added by majina 19-08-2021

			);
			array_push($arr, $packageAddOns_item);
		}
	  
		return $arr;

	}


	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				 package_code=:package_code, add_on_code=:add_on_code, 
				 add_on_name=:add_on_name, test_location_code=:test_location_code,
				 test_location_name=:test_location_name,
				 total_test_conducted=:total_test_conducted, 
				 patient_type_code=:patient_type_code
				 ";
					
	
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		
		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->add_on_code=htmlspecialchars(strip_tags($this->add_on_code));
		$this->add_on_name=htmlspecialchars(strip_tags($this->add_on_name));
		$this->test_location_code=htmlspecialchars(strip_tags($this->test_location_code));
		$this->test_location_name=htmlspecialchars(strip_tags($this->test_location_name));
		$this->total_test_conducted=htmlspecialchars(strip_tags($this->total_test_conducted));
		$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));

		
		// bind values
	
		$stmt->bindParam(":package_code", $this->package_code);
		$stmt->bindParam(":add_on_code", $this->add_on_code);
		$stmt->bindParam(":add_on_name", $this->add_on_name);
		$stmt->bindParam(":test_location_code", $this->test_location_code);
		$stmt->bindParam(":test_location_name", $this->test_location_name);
		$stmt->bindParam(":total_test_conducted", $this->total_test_conducted);
		$stmt->bindParam(":patient_type_code", $this->patient_type_code);

		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
		
	
	// used when filling up the update record form
	function readOne(){

		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->add_on_code=htmlspecialchars(strip_tags($this->add_on_code));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE	
					package_code =:package_code and
				    add_on_code = :add_on_code
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated by primary key, 2 PK
		$stmt->bindParam(":package_code", $this->package_code);
		$stmt->bindParam(":add_on_code", $this->add_on_code);

		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		

		// set values to object properties
		$this->package_code = $row['package_code'];
		$this->add_on_code = $row['add_on_code']; 
		$this->add_on_name= $row['add_on_name'];
		$this->test_location_code= $row['test_location_code'];
		$this->test_location_name= $row['test_location_name'];
		$this->total_test_conducted= $row['total_test_conducted'];
		$this->patient_type_code= $row['patient_type_code'];

	}


	// update the record except 2 primary key Code
	function update(){
		
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						 
						add_on_name=:add_on_name, test_location_code=:test_location_code, 
						test_location_name=:test_location_name,
						total_test_conducted=:total_test_conducted, 
						patient_type_code=:patient_type_code


					WHERE
					package_code=:package_code and add_on_code=:add_on_code";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
				
			$this->package_code=htmlspecialchars(strip_tags($this->package_code));
			$this->add_on_code=htmlspecialchars(strip_tags($this->add_on_code));
			$this->add_on_name=htmlspecialchars(strip_tags($this->pradd_on_nameice));
			$this->test_location_code=htmlspecialchars(strip_tags($this->test_location_code));
			$this->test_location_name=htmlspecialchars(strip_tags($this->test_location_name));
			$this->total_test_conducted=htmlspecialchars(strip_tags($this->total_test_conducted));
			$this->patient_type_code=htmlspecialchars(strip_tags($this->patient_type_code));
			

			
			// bind values

			$stmt->bindParam(":package_code", $this->package_code);
			$stmt->bindParam(":add_on_code", $this->add_on_code);
			$stmt->bindParam(":add_on_name", $this->add_on_name);
			$stmt->bindParam(":test_location_code", $this->test_location_code);
			$stmt->bindParam(":test_location_name", $this->test_location_name);
			$stmt->bindParam(":total_test_conducted", $this->total_test_conducted);
			$stmt->bindParam(":patient_type_code", $this->patient_type_code);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}



	// delete the record based on the primary key, add PK if more than 1
	function delete(){
		
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE add_on_code = ? and package_code = ?";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->package_code=htmlspecialchars(strip_tags($this->package_code));
		$this->add_on_code=htmlspecialchars(strip_tags($this->add_on_code));
		
		
		// bind id of record to delete
		$stmt->bindParam(2, $this->package_code);
		$stmt->bindParam(1, $this->add_on_code);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// search records
	function search($keywords){
		
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					add_on_code LIKE ? OR package_code LIKE ?  
				ORDER BY
				add_on_code";

		
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);
		$stmt->bindParam(2, $keywords);
	

		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}


}

?>