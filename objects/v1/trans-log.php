<?php


class TransLog{
  
    // database connection and table name
    private $conn;
    private $table_name = "transaction_log";

    // object properties
    public $id;
	public $trans_date;
	public $activity;
	public $username;
	public $status;
	 
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }
 
	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				activity=:activity,  username=:username, 
				status=:status ";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->activity=htmlspecialchars(strip_tags($this->activity));
		$this->username=htmlspecialchars(strip_tags($this->username));
		$this->status=htmlspecialchars(strip_tags($this->status));
	 
		
		// bind values
		$stmt->bindParam(":activity", $this->activity);
		$stmt->bindParam(":username", $this->username);
		$stmt->bindParam(":status", $this->status);

	 		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	 
}

?>