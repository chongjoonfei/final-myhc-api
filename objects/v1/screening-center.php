<?php

class ScreeningCenter{
  
    // database connection and table name
    private $conn;
    private $table_name = "screening_center";

    // object properties
    public $sc_id;
	public $sc_name;
	public $sc_address;
	public $sc_postcode;
	public $sc_state;
	public $sc_telephone;
	public $sc_person_in_charge;
	public $sc_picture_path;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
		
    }

    // read all records
	function readAll(){
	  
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				ORDER BY
					sc_id";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// execute query
		$stmt->execute();
	  
		return $stmt;
	}


	// used when filling up the update record form
	function readOne(){

		$this->sc_id=htmlspecialchars(strip_tags($this->sc_id));
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					sc_id = :sc_id
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":sc_id", $this->sc_id);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		// set values to object properties
		$this->sc_id = $row['sc_id'];
		$this->sc_name = $row['sc_name'];
		$this->sc_address = $row['sc_address'];
		$this->sc_postcode = $row['sc_postcode'];
		$this->sc_state = $row['sc_state'];
		$this->sc_telephone = $row['sc_telephone'];
		$this->sc_person_in_charge = $row['sc_person_in_charge'];
		$this->sc_picture_path = $row['sc_picture_path'];
	}

	// used when filling up the update record form
	function readById($id){

		$this->sc_id=htmlspecialchars(strip_tags($id));

		$record_item = null;
		
		// query to read single record
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					sc_id = :sc_id
				LIMIT
					0,1";
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind code of data to be updated
		$stmt->bindParam(":sc_id", $this->sc_id);
		
		// execute query
		$stmt->execute();
		
		// get retrieved row
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$record_item=array(
			"sc_id" => $row['sc_id'],
			"sc_name" => $row['sc_name'],
			"sc_address" =>  $row['sc_address'],
			"sc_postcode" =>  $row['sc_postcode'],
			"sc_state" =>  $row['sc_state'],
			"sc_telephone" => $row['sc_telephone'],
			"sc_person_in_charge" =>  $row['sc_person_in_charge'],
			"sc_picture_path" => $row['sc_picture_path']
		);

		return $record_item;
	}

	// create object
	function create(){
	  
		// query to insert record
		$query = "INSERT INTO
					" . $this->table_name . "
				SET
				sc_id=:sc_id,
				sc_name=:sc_name,  sc_address=:sc_address,  sc_postcode=:sc_postcode,  
				sc_state=:sc_state,  sc_telephone=:sc_telephone, sc_person_in_charge=:sc_person_in_charge,
				sc_picture_path=:sc_picture_path";
		
		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$this->sc_id=htmlspecialchars(strip_tags($this->sc_id));
		$this->sc_name=htmlspecialchars(strip_tags($this->sc_name));
		$this->sc_address=htmlspecialchars(strip_tags($this->sc_address));
		$this->sc_postcode=htmlspecialchars(strip_tags($this->sc_postcode));
		$this->sc_state=htmlspecialchars(strip_tags($this->sc_state));
		$this->sc_telephone=htmlspecialchars(strip_tags($this->sc_telephone));
		$this->sc_person_in_charge=htmlspecialchars(strip_tags($this->sc_person_in_charge));
		$this->sc_picture_path=htmlspecialchars(strip_tags($this->sc_picture_path));
		
		// bind values
		$stmt->bindParam(":sc_id", $this->sc_id);
		$stmt->bindParam(":sc_name", $this->sc_name);
		$stmt->bindParam(":sc_address", $this->sc_address);
		$stmt->bindParam(":sc_postcode", $this->sc_postcode);
		$stmt->bindParam(":sc_state", $this->sc_state);
		$stmt->bindParam(":sc_telephone", $this->sc_telephone);
		$stmt->bindParam(":sc_person_in_charge", $this->sc_person_in_charge);
		$stmt->bindParam(":sc_picture_path", $this->sc_picture_path);
		
		// execute query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	// update the record
	function update(){
	
		try{
			// update query
			$query = "UPDATE
						" . $this->table_name . "
					SET
						sc_name=:sc_name,  sc_address=:sc_address,  sc_postcode=:sc_postcode,  
						sc_state=:sc_state,  sc_telephone=:sc_telephone,  sc_person_in_charge=:sc_person_in_charge,
						sc_picture_path=:sc_picture_path
					WHERE
						sc_id = :sc_id";
			
			// prepare query statement
			$stmt = $this->conn->prepare($query);
				
			// sanitize
			$this->sc_id=htmlspecialchars(strip_tags($this->sc_id));
			$this->sc_name=htmlspecialchars(strip_tags($this->sc_name));
			$this->sc_address=htmlspecialchars(strip_tags($this->sc_address));
			$this->sc_postcode=htmlspecialchars(strip_tags($this->sc_postcode));
			$this->sc_state=htmlspecialchars(strip_tags($this->sc_state));
			$this->sc_telephone=htmlspecialchars(strip_tags($this->sc_telephone));
			$this->sc_person_in_charge=htmlspecialchars(strip_tags($this->sc_person_in_charge));
			$this->sc_picture_path=htmlspecialchars(strip_tags($this->sc_picture_path));
			
			// bind values
			$stmt->bindParam(":sc_id", $this->sc_id);
			$stmt->bindParam(":sc_name", $this->sc_name);
			$stmt->bindParam(":sc_address", $this->sc_address);
			$stmt->bindParam(":sc_postcode", $this->sc_postcode);
			$stmt->bindParam(":sc_state", $this->sc_state);
			$stmt->bindParam(":sc_telephone", $this->sc_telephone);
			$stmt->bindParam(":sc_person_in_charge", $this->sc_person_in_charge);
			$stmt->bindParam(":sc_picture_path", $this->sc_picture_path);
			
			// execute the query
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		
		}catch (Exception $e){
				echo "Error caught: " . $e->getMessage();
				return false;
		}
	}

	function delete(){

		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE sc_id = ?";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->sc_id=htmlspecialchars(strip_tags($this->sc_id));

		// bind id of record to delete
		$stmt->bindParam(1, $this->sc_id);

		// execute query
		if($stmt->execute()){
			return true;
		}
		return false;

	}

	// search records
	function search($keywords){
	
		// select all query
		$query = "SELECT
					*
				FROM
					" . $this->table_name . "  
				WHERE
					name LIKE ?  
				ORDER BY
					sc_name";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// sanitize
		$keywords=htmlspecialchars(strip_tags($keywords));
		$keywords = "%{$keywords}%";
		
		// bind
		$stmt->bindParam(1, $keywords);

		
		// execute query
		$stmt->execute();
		
		return $stmt;
	}
	
}

?>