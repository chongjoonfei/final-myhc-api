<?php
/**
 * Author: Elizha
 * PackageTestPanels.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-test-panels/create.php
 * JSON input: { "package_code":"<package_code>", "test_panel_code":"<test_panel_code>",
 * "test_location":"<test_location>",  "total_test_conducted":"<total_test_conducted>", "remark":"<remark>"}
 * Method: POST  
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/package-test-panels.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$packageTestPanels = new PackageTestPanels($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->package_code) &&
    !empty($data->test_panel_code ) 
){
  
    // set data property values
    $packageTestPanels->package_code = $data->package_code;
    $packageTestPanels->test_panel_code = $data->test_panel_code;
	$packageTestPanels->test_location = $data->test_location;
    $packageTestPanels->total_test_conducted = $data->total_test_conducted;
    $packageTestPanels->remark = $data->remark;


  
    // create the record
    if($packageTestPanels->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Package Test Panels info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		// $packageTestPanels->package_code = $data->package_code;
  		// read the details of record to be edited
		// $packageTestPanels->readOne();
		if (isset($packageTestPanels->test_panel_code)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Package Test Panels  info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Package Test Panels  info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Add On Services info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>