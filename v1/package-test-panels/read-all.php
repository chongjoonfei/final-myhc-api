<?php
/**
 * Author: Elizha
 * PackageTestPanels.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-test-panels/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-test-panels.php';
//include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$packageTestPanels = new PackageTestPanels($db);
//$testMarker = new TestMarker($db);

// query data
$stmt = $packageTestPanels->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $packageTestPanels_arr=array();
    $packageTestPanels_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $packageTestPanels_item=array(
            "package_code"  => $package_code,
            "test_panel_code"  => $test_panel_code ,
            "test_location" => $test_location,
            "total_test_conducted"  => $total_test_conducted,
            "remark" =>$remark
              

        );
  
        array_push($packageTestPanels_arr["data"], $packageTestPanels_item);
        $total_records++;
    }

    $packageTestPanels_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($packageTestPanels_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Package Test Panels found.","error" => "404 Not found")
    );
}
?>