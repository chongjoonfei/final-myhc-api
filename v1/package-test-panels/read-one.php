<?php
/**
 * Author: Elizha
 * PackageTestPanels.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-test-panels/read-one.php?c=<package_code>&p=<test_panel_code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-test-panels.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$packageTestPanels = new PackageTestPanels($db);
  
// set ID property of record to 
$packageTestPanels->package_code = isset($_GET['c']) ? $_GET['c'] : die();
$packageTestPanels->test_panel_code = isset($_GET['p']) ? $_GET['p'] : die();
  
// read the details of data to be edited
$packageTestPanels->readOne();
  
if (isset($packageTestPanels->test_panel_code)){
    // create array
    $packageTestPanels_arr = array(
        "package_code"  => $packageTestPanels->package_code,
        "test_panel_code"  => $packageTestPanels->test_panel_code ,
        "test_location" => $packageTestPanels->test_location,
        "total_test_conducted"  => $packageTestPanels->total_test_conducted,
        "remark" =>$packageTestPanels->remark

    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($packageTestPanels_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Package Test Panels info does not exist for " . $packageTestPanels->package_code,"error" => "404 Not found"));
}
?>