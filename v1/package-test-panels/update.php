<?php
/**
 * Author: Elizha
 * PackageTestPanels.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-test-panels/update.php 
 * JSON input: { "package_code":"<package_code>", "test_panel_code":"<test_panel_code>",
 * "test_location":"<test_location>",  "total_test_conducted":"<total_test_conducted>", "remark":"<remark>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-test-panels.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$packageTestPanels = new PackageTestPanels($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$packageTestPanels->package_code = $data->package_code;
$packageTestPanels->test_panel_code = $data->test_panel_code;
  
// set data property values
$packageTestPanels->test_location = $data->test_location;
$packageTestPanels->total_test_conducted = $data->total_test_conducted;
$packageTestPanels->remark = $data->remark;
  
// update the record
if($packageTestPanels->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Package Test Panels info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Package Test Panels info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>