<?php
/**
 * Author: Reeve
 * Company.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/city.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$city = new City($db);

// query data
$stmt = $city->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $postcode_arr=array();
    $postcode_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $postcode_data=array(
            "postcode" => $postcode,
			"area_name" => $area_name,
            "post_office" => $post_office,
            "state_code" => $state_code,
			

        );
  
        array_push($postcode_arr["data"], $postcode_data);
        $total_records++;
    }


    $postcode_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($postcode_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No City has found.","error" => "404 Not found")
    );
}
?>