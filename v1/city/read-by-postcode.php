<?php
/**
 * Author: Reeve
 * Company.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/city.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
 
// prepare object
$city = new City($db);
  
// set ID property of record to read
$city->postcode = isset($_GET['c']) ? $_GET['c'] : die();

  
// read the details of data to be edited


$arrResp = $city->readByPostcode($city->postcode);

  
// check if more than 0 record found


    if (isset($arrResp[0]['area_name'])){
  
    // record array
    
    // $postcode_arr = array(
    //     "post_office" => $city->post_office,
	// 	"postcode" => $city->postcode,
    //     "area_name" => $city->area_name,
    //     "state_code" => $city->state_code,
        
    // );
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($arrResp);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No City has found.","error" => "404 Not found")
    );
}
?>