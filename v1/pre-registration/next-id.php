<?php
/**
 * Author: Majina
 * PreRegistration.nextId()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/pre-registration/next-id.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/pre-registration.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$preRegistration = new PreRegistration($db);
  
// read the details of data to be edited
$nextId = $preRegistration->getNextId();

http_response_code(200);
echo json_encode(array("message" => "next id is " . $nextId,"error" => "200 OK"));
  

?>