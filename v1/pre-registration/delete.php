<?php
/**
 * Author: Majina
 * PreRegistration.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/pre-registration/delete.php
 * JSON input: { "ic_no":<ic_no>, "mobile_no":"<mobile_no>", "name": "<name>", "email":"<email>", "package_code":"<package_code>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/pre-registration.php';
include_once '../../objects/v1/registration-person.php';
include_once '../../objects/v1/user-account.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$preRegistration = new PreRegistration($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
$registrationPerson = new RegistrationPerson($db);
$registrationPerson->reg_no = $data->reg_no;

//todo: make sure to clear all accounts that registered under the package
//delete user-accounts
// $userAccount = new userAccount($db);
// $stmt_reg_person = $registrationPerson->readByRegNoAndPerson_Stmt($data->reg_no);
// while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
//     extract($row);
//     $userAccount->username = $username;
//     $userAccount->deleteByUsername();
// }
//delete registration-person
$registrationPerson->reg_no = $data->reg_no;
$registrationPerson->deleteByRegNo();

// set data to be deleted
$preRegistration->reg_no = $data->reg_no;

// delete the record
if($preRegistration->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "This application has been deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete application.","error"=>"503 service unavailable","errorFound"=>true));
}
?>