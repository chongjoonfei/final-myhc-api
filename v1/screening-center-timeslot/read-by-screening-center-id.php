<?php
/**
 * Author: Majina
 * ScreeningCenterTimeslot.readByScreeningCenterId()
 * URL for testing : https://myhc.my/myhc-api/v1/screening-center-timeslot/read-by-screening-center-id.php?c=<sc_id>
 * JSON input: none
 * Method: GET   
 */

error_reporting(E_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-center-timeslot.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$screeningCenterTimeslot = new ScreeningCenterTimeslot($db);
  
// set ID property of record to read
$screeningCenterTimeslot->sc_id = isset($_GET['c']) ? $_GET['c'] : die();
  
if (isset($screeningCenterTimeslot->sc_id)){

    // read the details of data to be edited
    $sct_arr = $screeningCenterTimeslot->readByScreeningCenterId($screeningCenterTimeslot->sc_id);
 
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($sct_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Screening Center's timeslot info does not exist for " . $screeningCenterTimeslot->sc_id,"error" => "404 Not found"));
}
?>