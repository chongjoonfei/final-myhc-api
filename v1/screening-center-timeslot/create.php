<?php
/**
 * Author: Majina
 * ScreeningCenterTimeslot.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-center-timeslot/create.php
 * JSON input: { "ts_code":"<ts_code>", "ts_code":"<ts_description>"}
 * Method: POST   
 */
     

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/screening-center-timeslot.php';
  
$database = new Database();
$db = $database->getConnection();
  
$screeningCenterTimeslot = new ScreeningCenterTimeslot($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->ts_code) && !empty($data->sc_id) 
){
  
    // set data property values
    $screeningCenterTimeslot->ts_code = $data->ts_code;
	$screeningCenterTimeslot->sc_id = $data->sc_id;
 
  
    // create the record
    if($screeningCenterTimeslot->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Screening Center's timeslot info has been created.","errorFound"=>false,"error" => ""));
    }
  
    // if unable to create record, tell the user
    else{
		$screeningCenterTimeslot->ts_id = $data->ts_id;
  		// read the details of patient to be edited
		$timeslot->readOne();
		if (isset($screeningCenterTimeslot->ts_id)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Screening Center's timeslot info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Screening Center's timeslot info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Screening center info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>