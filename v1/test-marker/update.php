<?php
/**
 * Author: Majina
 * testMarker.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-marker/update.php
 * JSON input: { "test_panel_code":<test_panel_code>, "code":"<code>", "name": "<name>", "description":"<description>, "unit":"<unit>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-marker.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$testMarker = new testMarker($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$testMarker->code = $data->code;
  
// set data property values
$testMarker->name = $data->name;
$testMarker->description = $data->description;
$testMarker->unit = $data->unit;
$testMarker->data_format = $data->data_format;
  
// update the record
if($testMarker->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Test Marker info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Test Marker info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>