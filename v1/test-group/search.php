<?php
/**
 * Author: Majina
 * TestGroup.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-group/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/test-group.php';
  
// instantiate database and object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$testGroup = new TestGroup($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $testGroup->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $testGroup_arr=array();
    $testGroup_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $testGroup_rec=array(
            "test_group_code" => $test_group_code,
            "test_panel_code" => $test_panel_code,
            "group_name" => $group_name,
            "patient_type" => $patient_type,
            "price" => $price,
            "enabled" => $enabled
        );
  
        array_push($testGroup_arr["data"], $testGroup_rec);
        $total_records++;
    }
  
    $testGroup_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($testGroup_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No test group found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>