<?php
/**
 * Author: Majina
 * TestGroup.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-group/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-group.php';
include_once '../../objects/v1/test-panel.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$testGroup = new TestGroup($db);
$testPanel = new TestPanel($db);
  
// set ID property of record to read
$testGroup->test_group_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$testGroup->readOne();
  
if (isset($testGroup->test_group_code)){
    // create array
    $testGroup_arr = array(
        "test_group_code" =>  $testGroup->test_group_code,
        "package_category" => $testGroup->package_category,
        "group_name" => $testGroup->group_name ,
        "patient_type" => $testGroup->patient_type,
        "price" => $testGroup->price,
        "enabled" => $testGroup->enabled,
        "test_panels" => $testPanel->readByTestGroupCode($testGroup->test_group_code)
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($testGroup_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Test Group info does not exist for " . $testGroup->test_group_code,"error" => "404 Not found"));
}
?>