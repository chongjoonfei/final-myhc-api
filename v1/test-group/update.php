<?php
/**
 * Author: Majina
 * TestGroup.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-group/update.php
 * JSON input: { "test_group_code":null, "test_panel_code":"<test_panel_code>", "group_name": "<group_name>", 
 *              "patient_type":"<patient_type>","price":"<price>","enabled":"<enabled>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-group.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$testGroup = new TestGroup($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$testGroup->test_group_code = $data->test_group_code;
  
// set data property values
$testGroup->group_name = $data->group_name;
$testGroup->test_group_code = $data->test_group_code;
$testGroup->patient_type = $data->patient_type;
$testGroup->price = $data->price;
$testGroup->enabled = $data->enabled;

 
// update the record
if($testGroup->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Test Group info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Test Group info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>