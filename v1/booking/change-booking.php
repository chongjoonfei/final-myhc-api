<?php
/**
 * Author: Majina
 * Booking.changeBooking()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/create-new-charge.php
 * JSON input: { "booking_no":"<booking_no>", "reg_no":"<reg_no>", "ic_no":"<ic_no>", 
 * "screening_location":"<screening_location>", "screening_center_id":"<screening_center_id>", "screening_date":"<screening_date>" ,
 * "screening_time":"<screening_time>" }
 *  
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");



ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);


if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {    
    return 0;    
} 
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/constant.php';
include_once '../../objects/v1/email-mgmt.php';
include_once '../../objects/v1/booking-product.php';
  
$database = new Database();
$db = $database->getConnection();
  
$booking = new Booking($db);
$BookingProduct = new BookingProduct($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    isset($data->booking_id) &&
    (isset($data->screening_location) || isset($data->screening_center_id))
    ){
        $booking->booking_no = $data->booking_no;
        $booking->readOne();

        if (isset($booking->booking_id)){


            $displayBooking = new Booking($db);
            $displayBooking->booking_no = $data->booking_no;
            $result = $displayBooking->readByBookingNo();

            $old_visit = $result['screening_location']!='HOME'?"Screening Centre" :"Home Visit";
            $old_venue = $result['screening_center']['sc_name'];
            $old_booking_datetime = $result['screening_date'].', '.$result['screening_time'];



            $booking->screening_center_id = $data->screening_center_id;
            $booking->screening_location = $data->screening_location;
            $booking->update();

            $products  = $data->products;

            foreach ($products as $product) {
                $bookingProduct = new BookingProduct($db);
                $bookingProduct->booking_id =  $data->booking_id;
                $bookingProduct->code = $product->code;
                $bookingProduct->name = $product->name;
                $bookingProduct->patient_type = $product->patient_type;
                $bookingProduct->price = $product->price;
                $bookingProduct->quantity = $product->quantity;
                $bookingProduct->total_price = $product->total_price;
                $bookingProduct->remark = $product->remark;
                $bookingProduct->ref_no = $booking->booking_no .'#2';
                $bookingProduct->create();
            }

            $displayBooking = new Booking($db);
                    $displayBooking->booking_no = $data->booking_no;
                    $result = $displayBooking->readByBookingNo();

                    
                    $new_changes = 
"New Changes : VENUE
Location (".($result['screening_location']!='HOME'?"Screening Centre" :"Home Visit").") : {$result['screening_center']['sc_name']}";

                    $emailMgmt = new EmailMgmt($db);
                    $emailMgmt->code = "booking-update";
                    $emailMgmt->readOne();
                    //$result['person']['email'] = "arslan@skuire.com";
                    $adminEm = CONST_ADMIN_EMAIL;
                    //$adminEm = 'arslanulhaq319@gmail.com';
                    $emailMgmt->receivers = $adminEm.",".$result['person']['email'];
                    /*$dt = new DateTime($result['date_modified'], new DateTimeZone('UTC'));
                    // change the timezone of the object without changing its time
                    $dt->setTimezone(new DateTimeZone("Asia/Kuala_Lumpur"));
                    // format the datetime
                    $result['date_modified'] = $dt->format('Y-m-d H:i:s');*/
                    $emailMgmt->paramValues =  array( 
                        array("param" => "?booking_no", "value" =>  $result['booking_no']),
                        array("param" => "?username", "value" =>  $result['userAccount']['username']),
                        array("param" => "?name", "value" => $result['person']['name']),
                        array("param" => "?ic_no", "value" => $result['person']['ic_no']),
                        array("param" => "?new_changes", "value" => $new_changes),
                        array("param" => "?venue", "value" => $old_venue),
                        array("param" => "?visit", "value" => $old_visit),
                        
                        array("param" => "?price", "value" => $result['products'][0]['price']),
                        array("param" => "?package", "value" => $result['products'][0]['name']),
                        array("param" => "?change_time", "value" => $result['date_modified']),
                        array("param" => "?booking_datetime", "value" => $old_booking_datetime)
                   );

                   $emailMgmt->sendMail();


            // set response code - 201 created
            http_response_code(201);
    
            // tell the user
            echo json_encode(array("message" => "Booking info has been updated.","errorFound"=>false,"error" => "","content"=>$booking));

        }else{
            http_response_code(400);
  
            // tell the user
            echo json_encode(array("message" => "Unable to update booking info.","errorFound"=>true,"error" => "400 bad request"));
        
        }
  
 
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update booking info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>