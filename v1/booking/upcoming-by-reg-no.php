<?php
/**
 * Author: Majina
 * booking.readByRegNo()
 * URL for testing : https://myhc.my/myhc-api/v1/booking/read-by-reg-no.php?c=<reg_no>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking.php';
include_once '../../objects/v1/booking-product.php';
include_once '../../objects/v1/booking-patient.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$booking = new Booking($db);
$bookingProduct = new BookingProduct($db);
$bookingPatient = new BookingPatient($db);
  
// set ID property of record to read
$booking->reg_no = isset($_GET['c']) ? $_GET['c'] : die();
  
if($booking->reg_no!=null){

    $arr = $booking->readUpcomingByRegNo($booking->reg_no);

    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Booking info does not exist for " . $booking->reg_no,"error" => "404 Not found"));
}
?>