<?php
/**
 * Author: Elizha
 * ContentMgmt.create()
 * URL for testing : https://myhealthcard.my/myhc-api/v1/content-mgmt/create.php
 * JSON input: { "id":null, "name": "<name>", "content":"<content>", "date_updated":"<date_updated>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/content-mgmt.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$contentMgmt = new ContentMgmt($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->name) &&
    !empty($data->content) 
){
    
    // set data property values
    //$contentMgmt->id = $data->id;  
    $contentMgmt->name = $data->name;
    $contentMgmt->content = $data->content;
	$contentMgmt->date_updated = $data->date_updated;

  
    // create the record
    if($contentMgmt->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Content Management info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		 $contentMgmt->name = $data->name;
  		// read the details of record to be edited
		 $contentMgmt->readOne();
		if($contentMgmt->content!=null){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Content Management  info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Content Management  info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Content Management  info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>