<?php
/**
 * Author: Majina
 * SmsNotification.sendMessage()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/sms-notification/send-message.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/sms-notification.php';
include_once '../../objects/v1/sms-log.php';
include_once '../../objects/v1/constant.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();

$urlSmsGateway = "http://dumei.bulksms2u.com/websmsapi/ISendSMS.aspx";


  
// prepare object
$smsNotification = new SmsNotification($db);
  
// set ID property of record to read
$data = json_decode(file_get_contents("php://input"));


$smsNotification->code = $data->code;
$paramValues = $data->paramValues;
  
// read the details of data to be edited
$smsNotification->readOne();


  
if (isset($smsNotification->code)){


    $msg = $smsNotification->message;

    foreach ($paramValues as $paramValue) {
        $msg = str_replace($paramValue->param,$paramValue->value,$msg);
    }
  
    //The data you want to send via POST
    $fields = [
        'username' => CONST_SMS_USERNAME,
        'password' => CONST_SMS_PASSWORD,
        'message' => $msg,
        'mobile' => $data->mobile,
        'sender' => 'test',
        'type' => '1'
    ];
   
    $response = httpPost($urlSmsGateway, $fields);
    
    //insert to sms_log
    $smsLog = new SmsLog($db);
    $smsLog->mobile_no = $data->mobile;
    $smsLog->message = $response;
    $smsLog->create();

    // send the message
    
    if(strpos($response,'1701')!==false){
  
        // set response code - 200 ok
        http_response_code(200);
  
        // tell the user
        echo json_encode(array("message" => "SMS Notification has been sent to your mobile phone " . $data->mobile,"errorFound"=>false,"error" => ""));
    }
  
    else{
 
        // set response code - problem in sending messages
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => $response . " : Unable to send SMS Notification.","errorFound"=>true,"error" => "SMS not delivered"));

    }
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "SMS Notification info does not exist for " . $data->code,"error" => "404 Not found"));
}

function httpPost($url, $data)
{
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}
?>