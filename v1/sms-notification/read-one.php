<?php
/**
 * Author: Majina
 * SmsNotification.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/sms-notification/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/sms-notification.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$smsNotification = new SmsNotification($db);
  
// set ID property of record to read
$smsNotification->code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$smsNotification->readOne();
  
if (isset($smsNotification->code)){
 
    // create array
    $smsNotification_arr = array(
        "code" => $smsNotification->code,
		"message" => $smsNotification->message,
        "params" => $smsNotification->params
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($smsNotification_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "SMS Notification info does not exist for " . $smsNotification->code,"error" => "404 Not found"));
}
?>