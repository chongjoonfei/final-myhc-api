<?php
/**
 * Author: Majina
 * BookingPatient.delete()
 * URL for testing : https://myhc.my/myhc-api/v1/booking-patient/delete.php
 * JSON input: { "booking_id":"<booking_id>", "ic_no":"<ic_no>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/booking-patient.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$bookingPatient = new BookingPatient($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$bookingPatient->booking_id = $data->booking_id;
$bookingPatient->ic_no = $data->ic_no;
  
// delete the record
if($bookingPatient->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Booking's patient info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Booking's patient info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>