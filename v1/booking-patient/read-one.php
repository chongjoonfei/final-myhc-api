<?php
/**
 * Author: Majina
 * BookingPatient.readOne()
 * URL for testing : https://myhc.my/myhc-api/v1/booking-patient/read-one.php?c=<code>&b=<booking_id>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/booking-patient.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$bookingPatient = new BookingPatient($db);
$person = new Person($db);
  
// set ID property of record to read
$bookingPatient->ic_no = isset($_GET['c']) ? $_GET['c'] : die();
$bookingProduct->booking_id = isset($_GET['b']) ? $_GET['b'] : die();
  
// read the details of data to be edited
$bookingPatient->readOne();
  
if ($bookingPatient->booking_id!=null){
 
    // create array
    $arr = array(
        "booking_id" => $booking_id,
        "ic_no" => $ic_no,
        "patient" => $person->readByIcNo($ic_no)
    );
  
    
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Booking's patient info does not exist for " . $bookingProduct->booking_id,"error" => "404 Not found"));
}
?>