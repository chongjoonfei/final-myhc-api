<?php
/**
 * Author: Elizha
 * MessageBox.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/create.php
 * JSON input: { "message_id":"<message_id>", "sender": "<sender>", "receiver":"<receiver>", "subject":"<subject>", "content":"<content>", 
 * "headers":"<headers>", "date_sent":"<date_sent>","message_type_code":"<message_type_code>", ,
 * "status":"<status>", "attachment":"<attachment>", "message_root_id":"<message_root_id>"}
 *  Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$messageBox = new MessageBox($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$messageBox->message_id = $data->message_id;


// set data property values
$messageBox->message_id = $data->message_id;
$messageBox->status = $data->status;
 
  
// update the record
if($messageBox->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Message status has been updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update message status.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>