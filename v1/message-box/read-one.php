<?php
/**
 * Author: Elizha
 * MessageBox.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/read-one.php?c=<message_id>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$messageBox = new MessageBox($db);
$submessageBox = new MessageBox($db);

// set ID property of record to read
$messageBox->message_id = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$messageBox->readOne();
  
if (isset($messageBox->content)){
    // create array
    $messageBox_arr = array(
        "message_id"  => $messageBox->message_id,
        "sender"  => $messageBox->sender ,
        "receiver" => $messageBox->receiver,
        "sender_name"  => $messageBox->sender_name ,
        "receiver_name" => $messageBox->receiver_name,
        "subject"  => $messageBox->subject,
        "content" => $messageBox->content,
        "headers" => $messageBox->headers,
        "date_sent" => $messageBox->date_sent,
        "message_type_code" => $messageBox->message_type_code,
        "status"=> $messageBox->status,
        "attachment"=> $messageBox->attachment,
        "message_root_id"=> $messageBox->message_root_id,
        "submessages" => $submessageBox->readByMessageRootId($message_id)
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($messageBox_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Message info does not exist for " . $_GET['c'],"error" => "404 Not found"));
}
?>