<?php
/**
 * Author: Majina
 * screeningCenter.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-center/update.php
 * JSON input: { "sc_id":"<sc_id>", "sc_name":"<sc_name>", "sc_address":"<sc_address>, "sc_postcode":"<sc_postcode>", "sc_state":"<sc_state>", "sc_telephone":"<sc_telephone>", "sc_person-in-charge":"<sc_person-in-charge>", "sc_picture-path":"<sc_picture-path>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-center.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$screeningCenter = new ScreeningCenter($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$screeningCenter->sc_id = $data->sc_id;
  
// set data property values
$screeningCenter->sc_name = $data->sc_name;
$screeningCenter->sc_address = $data->sc_address;
$screeningCenter->sc_postcode = $data->sc_postcode;
$screeningCenter->sc_state = $data->sc_state;
$screeningCenter->sc_telephone = $data->sc_telephone;
$screeningCenter->sc_person_in_charge = $data->sc_person_in_charge;
$screeningCenter->sc_picture_path = $data->sc_picture_path;
  
// update the record
if($screeningCenter->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Screening center info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Screening center info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>