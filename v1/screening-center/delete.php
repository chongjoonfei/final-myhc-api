<?php
/**
 * Author: Majina
 * ScreeningCenter.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-center/delete.php
 * JSON input: { "id":"<sc_id>", "sc_name":"<sc_name>", "sc_address":"<sc_address>, "sc_postcode":"<sc_postcode>", "sc_state":"<sc_state>", "sc_telephone":"<sc_telephone>", "sc_person-in-charge":"<sc_person-in-charge>", "sc_picture-path":"<sc_picture-path>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/screening-center.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$screeningCenter = new ScreeningCenter($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$screeningCenter->sc_id = $data->sc_id;
  
// delete the record
if($screeningCenter->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Screening center info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Screening center info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>