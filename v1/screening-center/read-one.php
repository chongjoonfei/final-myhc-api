<?php
/**
 * Author: Majina
 * ScreeningCenter.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-center/read-one.php?id=<sc_id>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-center.php';
include_once '../../objects/v1/screening-center-timeslot.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$screeningCenter = new ScreeningCenter($db);
$centerTimeslot = new ScreeningCenterTimeslot($db);
  
// set ID property of record to read
$screeningCenter->sc_id = isset($_GET['id']) ? $_GET['id'] : die();
  
// read the details of data to be edited
$screeningCenter->readOne();
  
if (isset($screeningCenter->sc_name)){
 
    // create array
    $arr = array(
        "sc_id" => $screeningCenter->sc_id,
		"sc_name" => $screeningCenter->sc_name,
        "sc_address" => $screeningCenter->sc_address,
        "sc_postcode" => $screeningCenter->sc_postcode,
        "sc_state" => $screeningCenter->sc_state,
        "sc_telephone" => $screeningCenter->sc_telephone,
        "sc_person_in_charge" => $screeningCenter->sc_person_in_charge,
        "sc_picture_path" => $screeningCenter->sc_picture_path,
        "timeslots" => $centerTimeslot->readByScreeningCenterId($screeningCenter->sc_id)
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Screening center info does not exist for " . $screeningCenter->sc_id,"error" => "404 Not found"));
}
?>