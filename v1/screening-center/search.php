<?php
/**
 * Author: Majina
 * ScreeningCenter.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-center/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/screening-center.php';
  
// instantiate database and object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$screeningCenter = new ScreeningCenter($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $screeningCenter->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $arr=array();
    $arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $rec=array(
            "sc_id" => $sc_id,
            "sc_name" => $sc_name,
            "sc_address" => $sc_address,
            "sc_postcode" => $screeningCenter->sc_postcode,
            "sc_state" => $sc_state,
            "sc_telephone" => $sc_telephone,
            "sc_person_in_charge" => $sc_person_in_charge,
            "sc_picture_path" => $sc_picture_path
        );
  
        array_push($arr["data"], $rec);
        $total_records++;
    }
  
    $arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No screening center found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>