<?php
$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . "upload_files/";
// $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$fileName = $_FILES['fileToUpload']['name'];
$uploadOk = 1;
$fileNameCmps = explode(".", $fileName);
$fileExtension = strtolower(end($fileNameCmps));
$target_file = $target_dir . $_POST["filename"] .'.'.$fileExtension;

$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$errorFound = false;

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
  if($check !== false) {
    // $msg = "File is an image - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    $msg = "File is not an image.";
    $errorFound=true;
    $uploadOk = 0;
  }
}

// Check if file already exists
if (file_exists($target_file) && !$errorFound) {
    $msg = "File already exists.";
    $errorFound=true;
    $uploadOk = 0;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 3000000  && !$errorFound) {
    $msg = "Your file is too large exceeding maximum size of 3MB.";
    $errorFound=true;
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "pdf"  && !$errorFound) {
    $msg = "Only JPG, JPEG, PNG, GIF & PDF files are allowed.";
    $errorFound=true;
    $uploadOk = 0;
}

if (!$errorFound){
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file) ) {
    $msg = "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
    $errorFound=false;
  } else {
    $msg = "Sorry, your file is not uploaded. ";
    $errorFound=true;
  }
}else{
  $msg = "Sorry, there was an error uploading your file. " .$msg;
}

 

echo json_encode(array("message" => $msg,"errorFound" => $errorFound));
?>