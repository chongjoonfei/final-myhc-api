<?php
/**
 * Author: Majina
 * EmailMgmt.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/email-mgmt/update.php
 * JSON input: { "code":"<code>", "title":"<title>", "content": "<content>", "sender": "<sender>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-category.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$emailMgmt = new EmailMgmt($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$emailMgmt->code = $data->code;
  
// set data property values
$emailMgmt->title = $data->title;
$emailMgmt->content = $data->content;
$emailMgmt->sender = $data->sender;
  
// update the record
if($emailMgmt->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Email Mgmt info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Email Mgmt info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>