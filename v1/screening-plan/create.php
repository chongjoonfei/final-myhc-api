<?php
/**
 * Author: Elizha
 * ScreeningPlan.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-plan/create.php
 * JSON input: { "package_code":"<package_code>", "single_package": "<single_package>", "category_code":"<category_code>", 
 * "picture_path":"<picture_path>" , "price":"<price>",
 * "license_validity_year":"<license_validity_year>", "test_included":"<test_included>", "note":"<note>" }
 * Method: POST   
 */
     
ini_set('display_errors',1);
error_reporting(E_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/screening-plan.php';
  
$database = new Database();
$db = $database->getConnection();

// prepare object
$screeningPlan = new ScreeningPlan($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->package_code) &&
    !empty($data->single_package) 
){


    // set data property values
    $screeningPlan->package_code = $data->package_code;
    $screeningPlan->single_package = $data->single_package;
	$screeningPlan->category_code = $data->category_code;
    $screeningPlan->description = $data->description;
    $screeningPlan->picture_path = $data->picture_path;
    $screeningPlan->price = $data->price;
    $screeningPlan->license_validity_year = $data->license_validity_year;
    $screeningPlan->test_included = $data->test_included;
    $screeningPlan->note = $data->note;   
    $screeningPlan->commercial = $data->commercial;   

    // create the record
    if($screeningPlan->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Screening Packages info was created.","errorFound"=>false,"error" => "",));
    }
  
      

    // if unable to create record, tell the user
    else{
		// $screeningPlan->package_code = $data->package_code;
  		// read the details of record to be edited
		// $screeningPlan->readOne();
		if (isset($screeningPlan->name)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Screening Packages info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Screening Packages info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Add On Services info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>