<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Majina
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-plan/upload-files.php
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/screening-plan.php';

$database = new Database();
$db = $database->getConnection();
  
$screeningPlan = new ScreeningPlan($db);

$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . "upload_files/package/";
$uploadOk = 1;

$package_code = $_POST["package_code"];

$errorFound = false;

// Check if file already exists
if (!isset($package_code) ) {
  $msg = "Data incomplete - " . $package_code;
  $errorFound=true;
}

// Check if file already exists
// if (file_exists($target_file) && !$errorFound) {
//     $msg = "File already exists. Please remove the existing uploaded file before upload";
//     $errorFound=true;
// }

// Check file size
// if ($_FILES["fileToUpload"]["size"] > 3072000  && !$errorFound) {
//     $msg = "Your file is too large exceeding maximum size of 3MB.";
//     $errorFound=true;
// }

// Allow certain file formats
// if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
// && $imageFileType != "gif" && $imageFileType != "pdf"  && !$errorFound) {
//     $msg = "Only JPG, JPEG, PNG, GIF & PDF files are allowed.";
//     $errorFound=true;
// }

if (!$errorFound){
  $file_names = $_FILES["fileToUpload"]['name'];

  for ($i = 0; $i < sizeof($file_names); $i++) {
    $fileName = $file_names[$i];
    
    $newFilename = $package_code .'_'.str_replace(' ','_',$fileName);
    $target_file = $target_dir . $newFilename;

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file) ) {

        $screeningPlan->package_code =  $package_code;
        $screeningPlan->picture_path = $newFilename;
        $screeningPlan->updatePhoto();
        
        $msg = "The file ". htmlspecialchars( basename($fileName)). " has been uploaded.";
        $errorFound=false;
    } else {
      $msg = "Sorry, your file is not uploaded. ";
      $errorFound=true;
    }
  }
} 

echo json_encode(array("message" => $msg,"errorFound" => $errorFound));
?>