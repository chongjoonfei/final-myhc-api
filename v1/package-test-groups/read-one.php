<?php
/**
 * Author: Elizha
 * PackageTestGroups.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-test-groups/read-one.php?c=<package_code>&p=<test_group_code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-test-groups.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$packageTestGroups = new PackageTestGroups($db);
  
// set ID property of record to 
$packageTestGroups->package_code = isset($_GET['c']) ? $_GET['c'] : die();
$packageTestGroups->test_group_code = isset($_GET['p']) ? $_GET['p'] : die();
  
// read the details of data to be edited
$packageTestGroups->readOne();
  
if (isset($packageTestGroups->test_group_code)){
    // create array
    $packageTestGroups_arr = array(
        "package_code"  => $packageTestGroups->package_code,
        "test_group_code"  => $packageTestGroups->test_group_code ,
        "test_location" => $packageTestGroups->test_location,
        "total_test_conducted"  => $packageTestGroups->total_test_conducted,
        "remark" =>$packageTestGroups->remark

    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($packageTestGroups_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Package Test Groups info does not exist for " . $packageTestGroups->package_code,"error" => "404 Not found"));
}
?>