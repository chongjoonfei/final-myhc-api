<?php
/**
 * Author: Majina
 * SmsNotification.readOne()
 * URL for testing : https://myhc.my/myhc-api/v1/payment/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/payment.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$payment = new Payment($db);
  
// set ID property of record to read
$payment->payment_id = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$array->readOne();
  
if (isset($payment->payment_id)){
 
    // create array
    $payment = array(
        "trans_no"  => $trans_no,
        "payment_id"  => $payment_id,
        "payment_date"  => $payment_date,
        "booking_no"  => $booking_no,
        "ref_no"  => $ref_no,
        "amount"  => $amount,
         "status"  => $status,
        "remark"  => $remark,
 
    );
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($array);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Payment info does not exist for id " . $_GET['c'],"error" => "404 Not found"));
}
?>