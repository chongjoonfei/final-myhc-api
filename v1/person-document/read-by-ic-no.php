<?php
/**
 * Author: Majina
 * PersonDocument.readByIcNo()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person-document/read-by-ic-no.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/person-document.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$personDocument = new PersonDocument($db);
  
// set ID property of record to read
$personDocument->ic_no = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$result = $personDocument->readByIcNo($personDocument->ic_no);
  
// if(sizeof($result)>0){
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    // echo json_encode($result);
    echo json_encode(array("data" => $result,"errorFound"=>false,"error" => null));
// }else{
    // set response code - 404 Not found
    // http_response_code(200);
  
    // tell the user that record does not exist
    // echo json_encode(array("message" => "Person document does not exist for " . $personDocument->ic_no,"errorFound"=>true,"error" => "Not found"));
// }
?>