<?php

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

/**
 * Author: Majina
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person-document/upload.php
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/person-document.php';
include_once '../../objects/v1/person.php';

$database = new Database();
$db = $database->getConnection();
  
$personDocument = new PersonDocument($db);
$person = new Person($db);

//$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . "upload_files/";

$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/" . "upload_files/";
// $fileName = $_FILES['fileToUpload']['name'];
$uploadOk = 1;

$ic_no = $_POST["ic_no"];
$document_code = $_POST["document_code"];

// $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$errorFound = false;

// Check if file already exists
if (!isset($ic_no) && !isset($document_code)) {
  $msg = "Data incomplete - " . $ic_no .' , '. $document_code;
  $errorFound=true;
}

// Check if file already exists
// if (file_exists($target_file) && !$errorFound) {
//     $msg = "File already exists. Please remove the existing uploaded file before upload";
//     $errorFound=true;
// }

// Check file size
// if ($_FILES["fileToUpload"]["size"] > 3072000  && !$errorFound) {
//     $msg = "Your file is too large exceeding maximum size of 3MB.";
//     $errorFound=true;
// }

// Allow certain file formats
// if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
// && $imageFileType != "gif" && $imageFileType != "pdf"  && !$errorFound) {
//     $msg = "Only JPG, JPEG, PNG, GIF & PDF files are allowed.";
//     $errorFound=true;
// }

if (!$errorFound){
  $file_names = $_FILES["fileToUpload"]['name'];

  for ($i = 0; $i < sizeof($file_names); $i++) {
    $fileName = $file_names[$i];
    
    // $fileNameCmps = explode(".", $fileName);
    // $fileExtension = strtolower(end($fileNameCmps));
    // $newFilename = $ic_no .'_'.$document_code.'.'.$fileExtension;
    $newFilename = $ic_no .'_'.str_replace(' ','_',$fileName);
    $target_file = $target_dir . $newFilename;

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file) ) {

      $personDocument->ic_no = $ic_no;
      $personDocument->document_code =$document_code;
      $personDocument->filename = $newFilename;
      $personDocument->ori_filename = $fileName;
      $personDocument->file_path = $target_file;

      if ($personDocument->create()){

        if ($document_code=='photo'){
          $person->ic_no =  $ic_no;
          $person->photo_path = $newFilename;
          $person->updatePhoto();
        }
        
        $msg = "The file ". htmlspecialchars( basename($fileName)). " has been uploaded.";
        $errorFound=false;
      }else{
        $msg = "Sorry, there was an error uploading your file. ";
        // ."icno->".$personDocument->ic_no.', doccode->' .$personDocument->document_code
        // .',filename->'.$personDocument->filename.', orifilename->'.$personDocument->ori_filename.',filepath->'.$personDocument->file_path;
        $errorFound=true;
      }

    } else {
      $msg = "Sorry, your file is not uploaded. ";
      $errorFound=true;
    }
  }
} 

echo json_encode(array("message" => $msg,"errorFound" => $errorFound));
?>