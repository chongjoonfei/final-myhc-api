<?php
/**
 * Author: Majina
 * BookingProduct.create()
 * URL for testing : https://myhc.my/myhc-api/v1/booking-product/create.php
 * JSON input: { "booking_id":"<booking_id>", "code":"<code>", "name":"<name>", 
 * "price":"<price>", "quantity":"<quantity>", "total_price":"<total_price>" ,
 * "remark":"<remark>" }
 *  
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/booking-product.php';
  
$database = new Database();
$db = $database->getConnection();
  
$bookingProduct = new BookingProduct($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->booking_id) &&
    !empty($data->code) &&
    !empty($data->name) 
    ){
  
	$bookingProduct->booking_id = $data->booking_id;
    $bookingProduct->code = $data->code;
    $bookingProduct->name = $data->name;
    $bookingProduct->patient_type = $data->patient_type;
    $bookingProduct->price = $data->price;
    $bookingProduct->quantity =  $data->quantity;
    $bookingProduct->total_price =  $data->total_price;
    $bookingProduct->remark =  $data->remark; 
    $bookingProduct->ref_no =  $data->ref_no; 
  
    // create the record
    if($bookingProduct->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Booking's product info has been created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
  		// read the details of patient to be edited
		$bookingProduct->readOne();
		if($bookingProduct->booking_id!=null){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Booking's product info already exist","errorFound"=>true,"error" => "Conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Booking's product info.","errorFound"=>true,"error" => "Service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Booking's product info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>