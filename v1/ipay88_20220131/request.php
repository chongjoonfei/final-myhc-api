<?php
/**
 * Author: Majina
 * ipay88.request()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/sms-notification/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
// include_once '../../config/db.php';
// include_once '../../objects/v1/sms-notification.php';

include_once 'service.php';
  
// get database connection
// $database = new Database();
// $db = $database->getConnection();

$paymentGatewayURL = "https://payment.ipay88.com.my/ePayment/entry.asp";

//(MerchantKey & MerchantCode & RefNo & Amount & Currency)
//7g3tvlc14oM16399A001500MYR
//4F7CoDXKbdM35641A001100MYR

$sha256 = iPay88_signature("4F7CoDXKbdM35641A001100MYR");

//The data you want to send via POST
$fields = [
    'MerchantCode' => 'M35641',
    'PaymentId' => '',
    'RefNo' => 'A001',
    'Amount' => '1.00',
    'Currency' => 'MYR',
    'ProdDesc' => 'Booking No: IJKLMN, Sector',
    'UserName' => 'majina',
    'UserEmail' => 'majina.sulaiman@gmail.com',
    'UserContact'  => '60128571385',
    'Remark'=> 'testing',
    'Lang'=> 'UTF-8',
    'SignatureType' => 'SHA256',
    'Signature' => $sha256,
    'ResponseURL' => 'https://myhealthcard.my/myhc-api/v1/ipay88/response.php',
    'BackendURL' => 'https://myhealthcard.my/myhc-api/v1/ipay88/backend.php' 
];


// send the message
$response = httpPost($paymentGatewayURL, $fields);

echo $response;

// echo json_encode(array("response" => $response));

/*
  
// prepare object
$smsNotification = new SmsNotification($db);
  
// set ID property of record to read
$data = json_decode(file_get_contents("php://input"));
$smsNotification->code = $data->code;
$paramValues = $data->paramValues;
  
// read the details of data to be edited
$smsNotification->readOne();
  
if($smsNotification->code!=null){

    $msg = $smsNotification->message;

    foreach ($paramValues as $paramValue) {
        $msg = str_replace($paramValue->param,$paramValue->value,$msg);
    }
  
    //The data you want to send via POST
    $fields = [
        'TransactionType' => 'SALE',
        'PymtMethod' => 'ANY',
        'ServiceID' => 'A00',
        'PaymentID' => 'ABCDEFGH130820142128',
        'OrderNumber' => 'IJKLMN',
        'PaymentDesc' => 'Booking No: IJKLMN, Sector',
        'MerchantName' => 'myhc',
        'MerchantReturnURL' => 'https://lamanbisnes.com/myhc-api/v1/payment/request.php',
        'MerchantCallbackURL'  => 'https://lamanbisnes.com/myhc-api/v1/payment/callback.php',
        'Amount'=> '228.00',
        'CurrenyCode'=> 'MYR',
        'CustIP' => '',
        'CustName' => '',
        'CustEmail' => '',
        'CustPhone' => '60128571385',
        'HashValue' => '',
        'MerchantTermsURL' => '',
        'LanguageCode' => 'en',
        'PageTimeout' => '780'
    ];

    // send the message
    $response = httpPost($paymentGatewayURL, $fields);
    if(strpos($response,'1701')!==false){
  
        // set response code - 200 ok
        http_response_code(200);
  
        // tell the user
        echo json_encode(array("message" => "SMS Notification has been sent to your mobile phone " . $data->mobile,"errorFound"=>false,"error" => ""));
    }
  
    else{
 
        // set response code - problem in sending messages
        http_response_code(503);

        // tell the user
        echo json_encode(array("message" => $response . " : Unable to send SMS Notification.","errorFound"=>true,"error" => "SMS not delivered"));

    }
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "SMS Notification info does not exist for " . $data->code,"error" => "404 Not found"));
}
*/

?>