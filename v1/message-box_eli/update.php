<?php
/**
 * Author: Elizha
 * MessageBox.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/message-box/update.php 
 * JSON input: { "message_inbox_code":"<message_inbox_code>", "sender": "<sender>", "receiver":"<receiver>", "subject":"<subject>", "message":"<message>", 
 * "headers":"<headers>", "date_sent":"<date_sent>","message_type_code":"<message_type_code>", "ic_no":"<ic_no>",
 * "status":"<status>", "attachment":"<attachment>"}
 *  Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/message-box.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$messageBox = new MessageBox($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$messageBox->add_on_code = $data->add_on_code;


// set data property values
$messageBox->message_inbox_code = $data->message_inbox_code;
$messageBox->sender = $data->sender;
$messageBox->receiver = $data->receiver;
$messageBox->subject = $data->subject; 
$messageBox->message = $data->message;
$messageBox->headers = $data->headers;
$messageBox->date_sent = $data->date_sent;
$messageBox->message_type_code = $data->message_type_code;
$messageBox->ic_no = $data->ic_no;
$messageBox->status = $data->status;
$messageBox->attachment = $data->attachment;
  
// update the record
if($messageBox->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Message info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Message info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>