<?php
/**
 * Author: Elizha
 * PackagePatient.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company/update.php 
 * JSON input: { "co_reg_no":"<co_reg_no>", "name":"<name>", "address":"<address>", 
 * "town":"<town>", "district":"<district>", "postcode":"<postcode>", "state":"<state>", 
 * "geocode":"<geocode>", "pic_url":"<pic_url>", "contact_no":"<contact_no>", "email":"<email>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/company.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$company = new Company($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited, 1 PK
$company->co_reg_no = $data->co_reg_no;

  
// set data property values
$company->name = $data->name;
$company->address = $data->address;
$company->town = $data->town;
$company->district = $data->district;
$company->postcode = $data->postcode;
$company->state = $data->state;
$company->geocode = $data->geocode;
$company->pic_url = $data->pic_url;
$company->contact_no = $data->contact_no;
$company->email = $data->email;

  
// update the record
if($company->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Company info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Company info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>