<?php
/**
 * Author: Majina
 * Company.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/company/create.php
 * JSON input: { "co_reg_no":"<co_reg_no>", "name":"<name>", "address":"<address>", 
 * "town":"<town>", "district":"<district>", "postcode":"<postcode>", "state":"<state>", 
 * "geocode":"<geocode>", "pic_url":"<pic_url>", "contact_no":"<contact_no>", "email":"<email>" }
 *  
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/company.php';
  
$database = new Database();
$db = $database->getConnection();
  
$company = new Company($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->co_reg_no) &&
    !empty($data->name) &&
    !empty($data->contact_no) &&
    !empty($data->email) 
    ){
  
    $company->co_reg_no = $data->co_reg_no;
	$company->name = $data->name;
    $company->address = $data->address;
    $company->town = $data->town;
    $company->district = $data->district;
    $company->postcode = $data->postcode;
    $company->state = $data->state;
    $company->geocode = $data->geocode;
    $company->pic_url = $data->pic_url;
    $company->contact_no = $data->contact_no;
    $company->email = $data->email;
  
    // create the record
    if($company->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Company info has been created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		//$company->co_reg_no = $data->co_reg_no;
  		// read the details of patient to be edited
		//$company->readOne();
		if(isset($company->co_reg_no)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Company info already exist","errorFound"=>true,"error" => "Conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create company info.","errorFound"=>true,"error" => "Service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create company info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>