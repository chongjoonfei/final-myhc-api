<?php

function iPay88_signature($source){
	return hash('sha256',$source);
}

function redirect($url) {
    ob_start();
    header('Location: '.$url);
    ob_end_flush();
    die();
}
?>