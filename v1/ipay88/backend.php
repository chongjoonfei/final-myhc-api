<?PHP

    ini_set('display_errors', 1); 
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    // get database connection
    include_once '../../config/db.php';
    include_once '../../objects/v1/ipay88-log.php';
	include_once '../../objects/v1/constant.php';
    include_once 'service.php';

    $database = new Database();
    $db = $database->getConnection();
	
	$merchant_key = CONST_IPAY88_MERCHANT_KEY;
 
    $merchantcode = $_REQUEST["MerchantCode"];
    $paymentid = $_REQUEST["PaymentId"];
    $refno = $_REQUEST["RefNo"];
    $amount = $_REQUEST["Amount"];
    $ecurrency = $_REQUEST["Currency"];
    $remark = $_REQUEST["Remark"];
    $transid = $_REQUEST["TransId"];
    $authcode = $_REQUEST["AuthCode"];
    $estatus = $_REQUEST["Status"];
    $errdesc = $_REQUEST["ErrDesc"];
    $signature = $_REQUEST["Signature"];
    $ccname = $_REQUEST["CCName"];
    $ccno = $_REQUEST["CCNo"];
    $s_bankname = $_REQUEST["S_bankname"];
    $s_country = $_REQUEST["S_country"];
 
   
    IF ($estatus==1) {
		// COMPARE Return Signature with Generated Response Signature
		// update order to PAID
		$sha256 = iPay88_signature($merchant_key.$merchantcode.$refno.str_replace(".", "", $amount).$ecurrency);
		
		if ($sha256==$signature || true) 
			echo "RECEIVEOK"; 
		else{
				 echo "SIGNATURE NOT MATCHED";
			}
    } ELSE {
		    
        // update order to FAIL
		$log = new Ipay88Log($db);
		$log->log_id = 0;
		$log->trans_id = $transid;
		$log->trans_date =  date('Y-m-d H:i:s');
		$log->trans_status = 'Error: ' . $errdesc . ' - response from iPay88';
		$log->payment_id = $paymentid;
		$log->ref_no = $refno;
		$log->amount = $amount;
		$log->params = "merchantcode = ".$merchantcode . 
			", paymentid = ".$paymentid . 
			", refno = ".$refno . 
			", amount = ".$amount . 
			", ecurrency = ".$ecurrency . 
			", remark = ".$remark . 
			", transid = ".$transid . 
			", authcode = ".$authcode . 
			", estatus = ". $estatus . 
			", errdesc = ".$errdesc . 
			", signature = ".$signature . 
			", ccname = ".$ccname . 
			", ccno = ".$ccno . 
			", s_bankname = ".$s_bankname . 
			", s_country = ".$s_country;
			$log->create();
    }


?>