<?php
/**
 * Author: Majina
 * UserAccount.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/update.php
 * JSON input: { "username":"<username>", "password":"<password>", "ic_no": "<ic_no>",
 * 			"acc_type_code":"<acc_type_code>","reg_no":"<reg_no>", "acc_status_code":"<acc_status_code>",
 * 			"date_created":"<date_created>", "date_updated":"<date_updated>"			
 * }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/user-account.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$userAccount = new UserAccount($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$userAccount->username = $data->username;
  
// set data property values
$userAccount->ic_no = $data->ic_no;
$userAccount->acc_type_code = $data->acc_type_code;
$userAccount->reg_no = $data->reg_no;
$userAccount->acc_status_code = $data->acc_status_code;
$userAccount->menu_owner = $data->menu_owner;
$userAccount->date_created = $data->date_created;
$userAccount->$date_updated = $data->date_updated;
  
// update the record
if($userAccount->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "User account has been updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update user account.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>