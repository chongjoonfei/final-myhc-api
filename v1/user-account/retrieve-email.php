<?php
/**
 * Author: Majina
 * UserAccount.reset-password()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/reset-password.php
 * JSON input: { "username":"<username>","ic_no":"<ic_no>" }"			
 * }
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {    
    return 0;    
} 
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/user-account.php';
include_once '../../objects/v1/email-mgmt.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/sms-notification.php';

include_once '../../objects/v1/trans-log.php';
  
$database = new Database();
$db = $database->getConnection();
  
$userAccount = new UserAccount($db);
$emailMgmt = new EmailMgmt($db);
$person = new Person($db);
$smsNotification = new SmsNotification($db);

$transLog = new TransLog($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


// make sure data is not empty
if (
    !empty($data->mobile) 
){
  
    // set data property values
    $person->mobile = $data->mobile;
	//$userAccount->ic_no = $data->ic_no;
	$person->readByMobile($data->mobile);


    print_r($person);

	// $person->ic_no =  $data->ic_no;
	// $person->readOne();

	
	// if (isset($userAccount->email) && isset($person->ic_no) && $userAccount->ic_no == $person->ic_no){
	// 	$password = $userAccount->randomPassword(8);
	// 	$userAccount->password = $userAccount->encryptPassword($password);
		
	// 	// create the record
	// 	if ($userAccount->changePasswordByEmail()){

			
			
	// 		//send sms notification
	// 		$smsNotification->code = "password-reset";
	// 		$smsNotification->readOne();
	// 		$smsNotification->mobile_no = "60". $person->mobile_no;
	// 		$smsNotification->paramValues =  array( 
	// 			array("param" => "?username", "value" => $userAccount->username),
	// 			array("param" => "?password", "value" => $password )
	// 		);
	// 		$smsNotification->sendSms();

	// 		//send email
	// 		$emailMgmt->code = "password-reset";
	// 		$emailMgmt->readOne();
	// 		$emailMgmt->receivers = $person->email;
	// 		$emailMgmt->paramValues = $smsNotification->paramValues;
			
	// 		if($_SERVER['HTTP_HOST']!="127.0.0.1")
    //                       {$emailMgmt->sendMail();}
	// 		// set response code - 200 Ok
	// 		http_response_code(200);
	// 		// tell the user
	// 		echo json_encode(array("person"=>$person,"message" => "User password has been reset.","errorFound"=>false,"error" => "",));

		 
	
	// 	}else{
	// 		// set response code - 400 bad request
	// 		http_response_code(400);
		
	// 		// tell the user
	// 		echo json_encode(array("message" => "Unable to reset password due to technical error","errorFound"=>true,"error" => "400 bad request"));
			
	// 		$transLog->activity="Forgot password request. Type=TechnichalError username={$data->username} ic.no={$data->ic_no} ip=".get_client_ip();
	// 		$transLog->username="";
	// 		$transLog->status="failed";
	// 		$transLog->create();
	// 	}
	// }else{
	// 	// set response code - 503 service unavailable
	// 	http_response_code(503);

	// 	// tell the user
	// 	echo json_encode(array("message" => "User account does not exist.","errorFound"=>true,"error" => "Service unavailable"));

	// 	$transLog->activity="Forgot password request. Type=Wrong Data username={$data->username} ic.no={$data->ic_no} ip=".get_client_ip();
	// 	$transLog->username="";
	// 	$transLog->status="failed";
	// 	$transLog->create();

	// }
  
 
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to retrieve Username/Email . Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
	$transLog->activity="Retrieve Username/Email request. Type=BadRequest username={$data->username} ic.no={$data->ic_no} ip=".get_client_ip();
	$transLog->username="";
	$transLog->status="failed";
	$transLog->create();
}
?>