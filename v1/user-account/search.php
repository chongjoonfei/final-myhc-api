<?php
/**
 * Author: Majina
 * UserAccount.search()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/user-account/search.php?s=<keyword>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once '../../config/core.php';
include_once '../../config/db.php';
include_once '../../objects/v1/user-account.php';
  
// instantiate database and patient object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$userAccount = new UserAccount($db);
  
// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";
  
// query data
$stmt = $userAccount->search($keywords);
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $userAccount_arr=array();
    $userAccount_arr["data"]=array();
  
    $total_records = 0;
    
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $userAccount_rec=array(
            "username" => $username,
			"ic_no" => $ic_no,
            "acc_type_code" => $acc_type_code, 
            "reg_no" => $reg_no, 
            "acc_status_code" => $acc_status_code,  
            "menu_owner" => $menu_owner,  
            "date_created" => $date_created, 
            "date_updated" => $date_updated, 
            "last_login" => $last_login, 
            "name" => $name,
            "email" => $email
        );
  
        array_push($userAccount_arr["data"], $userAccount_rec);
        $total_records++;
    }
  
    $userAccount_arr["total_records"]=$total_records;

    // set response code - 200 OK
    http_response_code(200);
  
    // show data
    echo json_encode($userAccount_arr);
}
  
else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not found
    echo json_encode(
        array("message" => "No username found.","errorFound"=>true,"error"=>"404 Not found")
    );
}
?>