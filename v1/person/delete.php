<?php
/**
 * Author: Majina
 * Person.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/delete.php
 * JSON input: { "reg_no":"<reg_no>", "ic_no":"<ic_no>", "name": "<name>",
 *               "gender":"<gender>", "age":"<age>", "email": "<email>",
 *               "mobile_no":"<mobile_no>", "patient_type_code":"<patient_type_code>", "address": "<address>",
 *               "town":"<town>", "district":"<district>", "postcode": "<postcode>", "state": "<state>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/person.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$person = new Person($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$person->ic_no = $data->ic_no;
  
// delete the record
if ($person->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Person info has been deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Person info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>