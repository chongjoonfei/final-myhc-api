<?php
/**
 * Author: Majina
 * Person.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/update.php
 * JSON input: { "reg_no":"<reg_no>", "ic_no":"<ic_no>", "name": "<name>",
 *               "gender":"<gender>", "age":"<age>", "email": "<email>",
 *               "mobile_no":"<mobile_no>", "patient_type_code":"<patient_type_code>", "address": "<address>",
 *               "town":"<town>", "district":"<district>", "postcode": "<postcode>", "state": "<state>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/person.php';
include_once '../../objects/v1/registration-person.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$person = new Person($db);
$registrationPerson = new RegistrationPerson($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$person->ic_no = $data->ic_no;
  
// set data property values
$person->name=$data->name;
$person->age=$data->age;
$person->email=$data->email;
$person->mobile_no=$data->mobile_no;
$person->gender=$data->gender;
$person->patient_type_code=$data->patient_type_code;
$person->address=$data->address;
$person->town=$data->town;
$person->district=$data->district;
$person->postcode=$data->postcode;
$person->state=$data->state;
$person->relationship=$data->relationship;



$person->citizen =$data->citizen;
$person->race =$data->race;
$person->religion =$data->religion;
$person->marital =$data->marital;
$dob = substr($data->dob,0,10);
$today = date("Y-m-d");
$agevalue = date_diff(date_create($dob), date_create($today));
$person->age = $agevalue->format('%y');

$person->date_of_birth = $dob; 

  
// update the record
if($person->update()){
  
    $registrationPerson->ic_no = $data->ic_no;
    $registrationPerson->reg_no = $data->reg_no;
    $registrationPerson->readOne();

    if (isset($registrationPerson->ic_no)){
        $registrationPerson->updatePatientTypeCodeByAge($data->age);
    }

    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Person info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Person info.","errorFound"=>true,"error"=>"503 service unavailable"));
}


?>