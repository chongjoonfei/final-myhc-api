<?php
/**
 * Author: Majina
 * Person.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/create.php
 * JSON input: { "reg_no":"<reg_no>", "ic_no":"<ic_no>", "name": "<name>",
 *               "gender":"<gender>", "age":"<age>", "email": "<email>",
 *               "mobile_no":"<mobile_no>", "patient_type_code":"<patient_type_code>", "address": "<address>",
 *               "town":"<town>", "district":"<district>", "postcode": "<postcode>", "state": "<state>"}
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/person.php';
  
$database = new Database();
$db = $database->getConnection();
  
$person = new Person($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));
  
// make sure data is not empty
if (
    !empty($data->ic_no) &&
    !empty($data->name) &&
    !empty($data->email) &&
    !empty($data->mobile_no)
){
  
    // set data property values
    // $person->reg_no=$data->reg_no;
    $person->ic_no=$data->ic_no;
    $person->name=$data->name;
    $person->age=$data->age;
    $person->email=$data->email;
    $person->mobile_no=$data->mobile_no;
    $person->gender=$data->gender;
    $person->patient_type_code=$data->patient_type_code;
    $person->address=$data->address;
    $person->town=$data->town;
    $person->district=$data->district;
    $person->postcode=$data->postcode;
    $person->state=$data->state;
    $person->relationship=$data->relationship;
  
    // create the record
    if($person->create()){
        $registrationPerson->ic_no = $data->ic_no;
        $registrationPerson->reg_no = $data->reg_no;
        $registrationPerson->readOne();
    
        if (isset($registrationPerson->ic_no)){
            $registrationPerson->updatePatientTypeCodeByAge($data->age);
        }
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "Person info was created.","errorFound"=>false,"error" => "",));
    }
  
    // if unable to create record, tell the user
    else{
		$person->ic_no = $data->ic_no;
  		// read the details of patient to be edited
		$person->readOne();
		if (isset($person->name)){
			// record already exist
			http_response_code(409);
  
			// tell the user
			echo json_encode(array("message" => "Person info already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create Person info.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create Person info. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}
?>