<?php
/**
 * Author: Majina
 * NcdProfile.delete()
 * URL for testing : https://myhc.my/myhc-api/v1/ncd-profile/delete.php
 * JSON input: { "ic_no":"<ic_no>", "tabacco_use":"<tabacco_use>", "alcohol_consumption":"<alcohol_consumption>, "diet_eat_fruit":"<diet_eat_fruit>", "diet_eat_vege":"<diet_eat_vege>", "physical_activities":"<physical_activities>", "history_hypertension":"<history_hypertension>", "history_diabetes":"<history_diabetes>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/ncd-profile.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$ncdProfile = new NcdProfile($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$ncdProfile->sc_id = $data->ic_no;
  
// delete the record
if($ncdProfile->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "NCD profile was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete NCD profile.","error"=>"503 service unavailable","errorFound"=>true));
}
?>