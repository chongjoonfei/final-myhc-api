<?php
/**
 * Author: Elizha
 * AddOn.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/add-on/update.php 
 * JSON input: { "add_on_code":"<add_on_code>", "name": "<name>", "price":"<price>", "remark":"<remark>", "status":"<status>", 
 * "unit":"<unit>", "unit_decimal":"<unit_decimal>","no_of_patient":"<no_of_patient>", "patient_type_code":"<patient_type_code>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/add-on.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$addOn = new AddOn($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$addOn->uid = $data->uid;

// set data property values
$addOn->add_on_code = $data->add_on_code;
$addOn->name = $data->name;
$addOn->price= $data->price;
$addOn->unit = $data->unit;
$addOn->unit_decimal=$data->unit_decimal;
$addOn->remark=$data->remark;
$addOn->status=$data->status;
$addOn->patient_type_code=$data->patient_type_code;
$addOn->no_of_patient=$data->no_of_patient;

  
// update the record
if($addOn->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Add On Services info was updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Add On Services info.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>