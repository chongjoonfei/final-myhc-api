<?php
/**
 * Author: Elizha
 * ScreeningPlan.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-plan/read-one.php?c=<package_code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/screening-plan.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$screeningPlan = new ScreeningPlan($db);
  
// set ID property of record to read
$package_code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$screeningPlan_arr=$screeningPlan->readByPackageCode($package_code);
  
// set response code - 200 OK
http_response_code(200);

// make it json format
echo json_encode($screeningPlan_arr);

/*if($screeningPlan->package_code!=null){
    // create array
    $screeningPlan_arr = array(
        "package_code"  => $screeningPlan->package_code,
        "single_package"  => $screeningPlan->single_package ,
        "category_code" => $screeningPlan->category_code,
        "picture_path" => $screeningPlan->picture_path,
        "price"  => $screeningPlan->price,
        "license_validity_year" => $screeningPlan->license_validity_year,
        "test_included" =>$screeningPlan->test_included,
        "application_license" =>$screeningPlan->application_license,
		"package_patients" => $screeningPlan->package_patient,
		"package_test_panels"=> $screeningPlan->package_test_panels,
		"package_add_ons"=> $screeningPlan->package_add_ons

    );
 
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($screeningPlan_arr);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Screening Packages info does not exist for " . $package_code,"error" => "404 Not found"));
}*/
?>