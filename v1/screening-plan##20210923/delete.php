<?php
/**
 * Author: Elizha
 * ScreeningPlan.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/screening-plan/delete.php
 * JSON input: { "package_code":"<package_code>", "single_package": "<single_package>", "category_code":"<category_code>", 
 * "picture_path":"<picture_path>", "price":"<price>",
 * "license_validity_year":"<license_validity_year>", "test_included":"<test_included>", "application_license":"<application_license>" }
 * Method: POST   
 */



// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/screening-plan.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$screeningPlan = new ScreeningPlan($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$screeningPlan->package_code = $data->package_code;
  


// delete the record
if($screeningPlan->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Screening Packages info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Screening Packages  info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>