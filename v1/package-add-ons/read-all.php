<?php
/**
 * Author: Elizha
 * PackageAddOns.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/package-add-ons/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/package-add-ons.php';
//include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$packageAddOns = new PackageAddOns($db);
//$testMarker = new TestMarker($db);

// query data
$stmt = $packageAddOns->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $packageAddOns_arr=array();
    $packageAddOns_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
  
        $packageAddOns_item=array(
            "package_code"  => $package_code,
            "add_on_code"  => $add_on_code ,
            "add_on_name" => $add_on_name,
            "test_location_code"  => $test_location_code,
            "test_location_name" => $test_location_name,
            "total_test_conducted" =>$total_test_conducted,
            "patient_type_code" =>$patient_type_code
              //"test_markers" => $testMarker->readByPanelCode($code)
        );
  

        array_push($packageAddOns_arr["data"], $packageAddOns_item);
        $total_records++;
    }

    $packageAddOns_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($packageAddOns_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "Package Add Ons found.","error" => "404 Not found")
    );
}
?>