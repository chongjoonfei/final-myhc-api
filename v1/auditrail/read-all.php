<?php
/**
 * Author: Elizha
 * AddOn.readAll()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/add-on/read-all.php
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");

// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/auditrail.php';
//include_once '../../objects/v1/test-marker.php';
  
// instantiate database and data object
$database = new Database();
$db = $database->getConnection();

// echo hash('sha256','123456');

// exit;
  
// initialize object
$auditrail = new Auditrail($db);
//$testMarker = new TestMarker($db);

// query data
$stmt = $auditrail->readAll();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // record array
    $addOn_arr=array();
    $addOn_arr["data"]=array();
    
    // retrieve our table contents
    $total_records = 0;
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        extract($row);
         
        $addOn_item=array(
            "id"  => $intAuditrailId,
            "strModuleNamePathTblName"  => $strModuleNamePathTblName,
            "intRecordId"  => $intRecordId ,
            "intAction" => $intAction,
            "strAddedByUsername"  => $strAddedByUsername,
            "strAddedByEmail" => $strAddedByEmail,           
            "status" =>$intStatus,
            "CreateDate"=>$CreateDate,
          
             
        );
  
        array_push($addOn_arr["data"], $addOn_item);
        $total_records++;
    }

    $addOn_arr["total_records"]=$total_records;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show data in json format
    echo json_encode($addOn_arr);
}else{
  
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user no record found
    echo json_encode(
        array("message" => "No Auditrail found.","error" => "404 Not found")
    );
}
?>