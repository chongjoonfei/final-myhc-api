<?php
/**
 * Author: Majina
 * Person.update()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/person/update.php
 * JSON input: { "reg_no":"<reg_no>", "ic_no":"<ic_no>", "person_type_code": "<person_type_code>" }
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/registration-person.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$registrationPerson = new RegistrationPerson($db);
  
// get id of record to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of record to be edited
$registrationPerson->ic_no = $data->ic_no;
  
// set data property values
$registrationPerson->reg_no=$data->reg_no;
$registrationPerson->person_type_code=$data->person_type_code;
  
// update the record
if ($registrationPerson->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Person registration has been updated.","errorFound"=>false,"error"=>"success"));
}
  
// if unable to update the record, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to update Person registration.","errorFound"=>true,"error"=>"503 service unavailable"));
}
?>