<?php
/**
 * Author: Majina
 * TestReferenceRange.update_all()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-reference-range/create.php
 * JSON input: [{ "test_marker_code":<test_marker_code>, "code":"<code>", "min": "<min>", "max":"<max>","summary":"<summary>"}]
 * Method: POST   
 */
     
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/test-reference-range.php';
  
$database = new Database();
$db = $database->getConnection();
  
$testReferenceRange = new TestReferenceRange($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"),true);
  
try {
    foreach ($data as $item) {
        // make sure data is not empty
        if (
            !empty($item['code']) &&
            !empty($item['test_marker_code']) 
        ){
        
            // set data property values
            $testReferenceRange->test_marker_code = $item['test_marker_code'];
            $testReferenceRange->code = $item['code'];
            $testReferenceRange->summary = $item['summary'];
            $testReferenceRange->min = $item['min'];
            $testReferenceRange->max = $item['max'];
        
            // create the record
            if ($testReferenceRange->create()){
        
                // set response code - 201 created
                http_response_code(201);
        
                // tell the user
                throw new Exception("Test reference range info was created.");
            }
        
            // if unable to create record, tell the user
            else{
                // read the details of record to be edited
                if (isset($testReferenceRange->code)){
                    // record already exist and let the record to be updated
                    if ($testReferenceRange->update()){
                        http_response_code(200);
                    }else{
                        http_response_code(503);
                        throw new Exception("Unable to create Test Panel info. Error in updating the information.");                   }
                }else{
                    // set response code - 503 service unavailable
                    http_response_code(400);
                    throw new Exception("Unable to create Test Panel info. Data is incomplete.");
                }

            }
        }
        
        // tell the user data is incomplete
        else{
        
            // set response code - 400 bad request
            http_response_code(400);
        
            // tell the user
            throw new Exception("Unable to create Test Panel info. Data is incomplete.");
        }
    }
            
    // tell the user
    echo json_encode(array("message" => "Test reference range info was updated.","errorFound"=>false,"error" => ""));
}catch(Exception $e){
    echo json_encode(array("message" => $e->getMessage(),"errorFound"=>true,"error" => "Error"));
}
?>