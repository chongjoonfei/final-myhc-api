<?php
/**
 * Author: Majina
 * TestReferenceRange.delete()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-reference-range/delete.php
 * JSON input: { "test_marker_code":<test_marker_code>, "code":"<code>", "min": "<min>", "max":"<max>","summary":"<summary>"}
 * Method: POST   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object file
include_once '../../config/db.php';
include_once '../../objects/v1/test-reference-range.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare class object
$testReferenceRange = new TestReferenceRange($db);
  
// get data input
$data = json_decode(file_get_contents("php://input"));
  
// set data to be deleted
$testReferenceRange->code = $data->code;
  
// delete the record
if($testReferenceRange->delete()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Test Reference Range info was deleted.","error"=>"","errorFound"=>false));
}
  
// if unable to delete the record
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Unable to delete Test Reference Range info.","error"=>"503 service unavailable","errorFound"=>true));
}
?>