<?php
/**
 * Author: Majina
 * TestPanel.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/test-panel/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/test-reference-range.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$testReferenceRange = new TestReferenceRange($db);
  
// set ID property of record to read
$testReferenceRange->code = isset($_GET['c']) ? $_GET['c'] : die();
  
// read the details of data to be edited
$testReferenceRange_arr = $testReferenceRange->readByTestMarkerCode($testReferenceRange->code);

http_response_code(200);

echo json_encode($testReferenceRange_arr);
  
// if($testReferenceRange->code!=null){
//     // create array
//     $testReferenceRange_arr = array(
//         "test_marker_code" =>  $testReferenceRange->test_marker_code,
//         "code" => $testReferenceRange->code,
//         "min" => $testReferenceRange->min ,
// 		"max" => $testReferenceRange->max,
//         "summary" => $testReferenceRange->summary
//     );
  
//     // set response code - 200 OK
//     http_response_code(200);
  
//     // make it json format
//     echo json_encode($testReferenceRange_arr);
// }else{
//     // set response code - 404 Not found
//     http_response_code(404);
  
//     // tell the user that record does not exist
//     echo json_encode(array("message" => "Test Reference Range info does not exist for " . $_GET['c'],"error" => "404 Not found"));
// }
?>