<?php
/**
 * Author: Majina
 * MenuItem.readOne()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/menu-item/read-one.php?c=<code>
 * JSON input: none
 * Method: GET   
 */

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
  
// include database and object files
include_once '../../config/db.php';
include_once '../../objects/v1/menu-main-item.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare object
$menuMainItem = new MenuMainItem($db);
  
// set ID property of record to read
$menuMainItem->item_id = isset($_GET['item_id']) ? $_GET['item_id'] : die();

$menuMainItem->main_id = isset($_GET['main_id']) ? $_GET['main_id'] : die();

  
// read the details of data to be edited

$adata = $menuMainItem->readCRUD();
 


if(isset($adata[0]['permission'])){
 
   
  
    // set response code - 200 OK
    http_response_code(200);
  
    // make it json format
    echo json_encode($adata[0]['permission']);
}else{
    // set response code - 404 Not found
    http_response_code(404);
  
    // tell the user that record does not exist
    echo json_encode(array("message" => "Menu item info does not exist for " . $_GET['c'] ,"error" => "404 Not found"));
}
?>