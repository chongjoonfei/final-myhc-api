<?php
/**
 * Author: Majina
 * DataSharing.create()
 * URL for testing : https://lamanbisnes.com/myhc-api/v1/data-sharing/create.php
 * JSON input: { "ic_no":"<ic_no>", "bmz_updates":"<bmz_updates>", "partner_updates":"<partner_updates>,}
 * Method: POST   
 */
     
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  

// get database connection
include_once '../../config/db.php';
  
// instantiate class object
include_once '../../objects/v1/data-sharing.php';
  
$database = new Database();
$db = $database->getConnection();
  
$dataSharing = new DataSharing($db);
  
// get posted data
$data = json_decode(file_get_contents("php://input"));

//echo json_encode(array($data));
 

// make sure data is not empty
//print_r($data);

if (
    !empty($data->ic_no) 
){
  
    // set data property values
    $dataSharing->ic_no = $data->ic_no;
    $dataSharing->bmz_updates = $data->bmz_updates;
    $dataSharing->partner_updates = $data->partner_updates;

    // create the record
    if($dataSharing->create()){
        // set response code - 201 created
        http_response_code(201);
  //echo "creta yes";
  
        // tell the user
        echo json_encode(array("message" => "Data Share has been created.","errorFound"=>false,"error" => ""));
    }
  
    // if unable to create record, tell the user
    else{
  //echo "creta no";
		$dataSharing->ic_no  = $data->ic_no;
  		// read the details of patient to be edited
		$dataSharing->readOne();
		if (isset($dataSharing->ic_no)){
			// record already exist
			http_response_code(409);
  //echo "creta found";
			// tell the user
			echo json_encode(array("message" => "already exist","errorFound"=>true,"error" => "409 conflict"));
		}else{
			echo "creta no1111";
			// set response code - 503 service unavailable
			http_response_code(503);
  
			// tell the user
			echo json_encode(array("message" => "Unable to create.","errorFound"=>true,"error" => "503 service unavailable"));
		}

    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create NCD profile. Data is incomplete.","errorFound"=>true,"error" => "400 bad request"));
}

 
?>